// FILE TO SOLVE THE GERTLER-KIYOTAKI MODEL
// WITH HETSOL TOOLKIT (Reiter 2015)
// int i, June 08 2015


// IMPERFECT INTERBANK MARKET





//Variables
VAR L ;
VAR K ;
VAR Y ;
VAR C ;
VAR C_llag ;
VAR C_lag ;
VAR I ;
VAR Iratio ;
VAR f ;
VAR fp ;
VAR R ;
VAR W ;
VAR Z ;
VAR z ;
VAR psi ;
VAR lpsi ;
SHOCK epsilon_z ;
SHOCK epsilon_psi ;
VAR G ;
VAR T ;
VAR Lambda ;
VAR Q(2) ;
VAR R_k(2) ;
VAR ER_k ;
VAR R_b ;
VAR spread ;
VAR V_s ;
VAR V_b ;
VAR V_d ;
VAR Omega(2) ;
VAR s(2) ;
VAR S ;
VAR n(2) ;
VAR N ;
VAR lam(2) ;
VAR lambdabar ;
VAR theta ;
VAR omega ;
VAR D ;
VAR D_h ;
VAR D_g ;
SHOCK epsilon_omega ;
SHOCK epsilon_theta ;
VAR b(2) ;
VAR phi ;
VAR spreadInter ;


//Parameters
PAR beta = 0.99;
PAR gam = 0.5;
PAR chi = 5.584;
PAR epsi = 0.1;
PAR alpha = 0.33;
PAR delta = 0.025;
PAR omegabar = 0.9;
PAR pi(2);
PARCALI thetabar;
PARCALI xi;
PAR sigma = 0.972;
PAR eta_I = 1.5;
PAR Gshare = 0.2;
PARCALI Gbar;
PAR rho_z = 0.66;
PAR rho_psi = 0.66;
PAR rho_omega = 0.66;
PAR rho_theta = 0.66;
PAR LR = 4;
PAR SPREAD = 0.0025;




SETPAR;
pi(1) = 0.25;
pi(2) = 0.75;


//// FUNCTIONS

// Goods producer
//# production function:
FUNC F(z,K,L) = exp(z)*pow(K,alpha)*pow(L,(1-alpha));
//# marginal productivity of capital:
FUNC F_k(z,K,L) = alpha*exp(z)*pow(L/K,1-alpha);
//# marginal productivity of labor:
FUNC F_L(z,K,L) = (1-alpha)*exp(z)*pow(K/L,alpha);

// Households 
//# Marginal utility of consumption:
FUNC uC(C, Clead, Clag) = pow(C - gam * Clag, -1) - beta * gam * EXP(pow(Clead - gam * C, -1));
//# Marginal disutility of labor:
FUNC u_L(L) = chi * pow(L,epsi);

FUNC ff(Iratio) = eta_I /2 * pow(Iratio - 1, 2);
FUNC ffp(Iratio) = eta_I * (Iratio - 1);


MODEL;
//# Exogenous equation for productivity:  
z[t] := rho_z * z[t-1] + epsilon_z;
//# Exogenous equation for quality of capital:  
lpsi[t] := rho_psi * lpsi[t-1] + epsilon_psi;
psi[t] := exp(lpsi[t]);


omega[t] := (1 - rho_omega) * omegabar + rho_omega * omega[t-1] + epsilon_omega;
theta[t] := (1 - rho_theta) * thetabar + rho_theta * theta[t-1] + epsilon_theta;



//# Production function
Y[t] := F(z[t],K[t],L[t] ); // Eq. [t+1]
//# Law of motion for capital:
K[t] := psi[t] * ( I[t-1] + (1 - delta) * K[t-1] ) ; // Eq (2)
Iratio[t] := I[t]/I[t-1]; // lowers # unstable roots

C_lag[t] := C[t-1];
C_llag[t] := C_lag[t-1];

/// HERE Try other timing of lambda!!!
Lambda: Lambda[t] = beta * uC(C[t], C[t+1], C_lag[t])/uC(C_lag[t], C[t], C_llag[t]);
euler:    1 = EXP(R[t] * Lambda[t+1]); // Eq (7)

//# Optimal labor input: wage = marginal productivity of labor: 
W[t] :=  F_L(z[t],K[t],L[t]); // Eq (38)
//# Optimal capital input: Dividend is marginal product of capital
mpc: Z[t] =  F_k(z[t],K[t],L[t]); // Eq (39)
//# Aggregate resource constraint:
ARC: Y[t] = C[t] + (1 + ff(I[t]/I[t-1]) ) * I[t] + G[t]; // Eq (3)

f[t] := eta_I /2 * pow(Iratio[t] - 1, 2);
fp[t] := eta_I * (Iratio[t] - 1);
Qinv: Q(1)[t] = 1 + ff(Iratio[t]) + Iratio[t] * ffp(Iratio[t]) - EXP( Lambda[t+1] * pow(Iratio[t+1], 2) * fp[t+1] ); // Eq (40)

macl1: s(1)[t] = I[t] + (1 - delta) * pi(1) * K[t]; // Eq (41)
macl2: s(2)[t] = (1 - delta) * pi(2) * K[t]; // Eq (41)
S[t] := s(1)[t] + s(2)[t]; // not in the paper

//# FOC for labor supply:
ls:  W[t] * EXP( uC(C[t], C[t+1], C_lag[t]) ) = u_L(L[t]); // Eq (6), Eq (42)

// Net worth 
N1: n(1)[t] = pi(1) * ( (xi + sigma) *  (Z[t] + (1 - delta) * Q(1)[t]) * psi[t] * S[t-1] - sigma * R[t-1] * D[t-1] ) ; // Eq (35)
N2: n(2)[t] = pi(2) *( (Z[t] + (1 - delta) * Q(2)[t]) * psi[t] * S[t-1] * (xi + sigma) - sigma * R[t-1] * D[t-1]); // Eq (34) (35) (36)
// N is total net worth (across islands)
N[t] := n(1)[t] + n(2)[t]; // not in the paper

D[t] := Q(1)[t] *  s(1)[t] + Q(2)[t] *  s(2)[t] - N[t]; // Eq (37) !!
// household debt = total deposits + supply of gov debt
D_h[t] := D[t] + D_g[t]; // AFTER Eq (42)
// Government
T[t] := G[t];
D_g[t] := 0;
G[t] := Gbar;

// CALIBRATIONS:
CALI @ LRstst: SUM(i, 1, 2, Q(i)[t] * s(i)[t]) = LR * N[t];
CALI @ GStSt: Gbar = Gshare * Y[t];
CALI @ SpreadStSt: ER_k[t] = R[t-1] + SPREAD;

//CALI @ SpreadStSt: R_k(1)[t] = R[t-1] + 0.01/4 ;

// Asset returns and spreads
// not in the paper: expected returns on each island
R_k1: R_k(1)[t] = psi[t]/Q(1)[t-1] * (Z[t] + (1 - delta) * SUM(i,1,2,pi(i) * Q(i)[t]));
R_k2: R_k(2)[t] = psi[t]/Q(2)[t-1] * (Z[t] + (1 - delta) * SUM(i,1,2,pi(i) * Q(i)[t]));

ER_k[t] := SUM(i, 1, 2, pi(i) * R_k(i)[t]);
spread[t] := EXP( ER_k[t+1] - R[t] );



// test
lam_1: lam(1)[t] * ( theta[t]*(1-omega[t]) - ( V_s[t]/Q(1)[t] - V_b[t]
)) = ( V_s[t]/Q(1)[t] - V_b[t] ); \\ eq:lam_1 eq:15

lam_2: ( theta[t]*(1-omega[t])* Q(2)[t] - ( V_s[t] - V_b[t]* Q(2)[t]
)) *lam(2)[t] = ( V_s[t] - V_b[t]*Q(2)[t] ); eq:lam_2 eq:15

lambdabar[t] := SUM(i, 1, 2, pi(i) * lam(i)[t] ); // eq:lambdabar

Omega_1: Omega(1)[t] = 1 - sigma + sigma * ( V_b[t] + lam(1)[t] * (
V_b[t] - theta[t] * omega[t])); //eq:Omega-gen
Omega_2: Omega(2)[t] = 1 - sigma + sigma * ( V_b[t] + lam(2)[t] * (
V_b[t] - theta[t] * omega[t])); //eq:Omega-gen

QS_1: ( theta[t] * ( 1 - omega[t]) - (V_s[t]/Q(1)[t] - V_b[t]) ) *
Q(1)[t]* s(1)[t] +  (theta[t] *omega[t] - (V_b[t] - V_d[t])) * pi(1)
*D[t] = (V_b[t] - theta[t]* omega[t]) * n(1)[t] ; // eq:agg-ICC (16)

QS_2: ( theta[t] * ( 1 - omega[t]) - (V_s[t]/Q(2)[t] - V_b[t]) ) *
Q(2)[t]* s(2)[t] +  (theta[t] *omega[t] - (V_b[t] - V_d[t])) * pi(2)
*D[t] = (V_b[t] - theta[t]* omega[t]) * n(2)[t] ; // eq:agg-ICC (16)


Eq73: (1+lambdabar[t])*(V_b[t]-V_d[t]) = lambdabar[t] * theta[t] *
omega[t]; // eq:14 Eq. (14)

V_b: V_d[t]*R_b[t] = R[t] * V_b[t]; // eq:Vd-gen  Eq. (78)
V_d[t] := EXP( Lambda[t+1] * SUM(i, 1, 2, pi(i) * Omega(i)[t+1] * R[t]
)); // eq:Vd-gen Eq (29)
V_s[t] := SUM(i, 1, 2, pi(i) * Lambda[t+1] * Omega(i)[t+1] * (Z[t+1] +
(1-delta) * Q(i)[t+1] ) * psi[t+1] ); // eq:Vs-gen   Eq. (79)


// JUST FOR THE ANALYSIS: What is the size of the interbank market?

b_i:	 b(1)[t] = Q(1)[t] * s(1)[t] - n(1)[t] - D[t]*pi(1);
b_n:	 b(2)[t] = Q(2)[t] * s(2)[t] - n(2)[t] - D[t]*pi(2);

phi[t] := SUM(i, 1, 2, Q(i)[t] * s(i)[t]) / N[t];

spreadInter[t] := R_b[t] - R[t];
STST;
GUESS L:ls = [0.01 : 0.99]@0.239857618965094;
GUESS Q(2):QS_2 = [0.95 : 1.5]@1.01490321099252;
GUESS Omega(1):Omega_1 = [-0.5 : 2.5]@1.93185068268329;
GUESS Omega(2):Omega_2 = [-0.5 : 2]@1.49842946403249;
GUESS Z:R_k1 = [0.001 : 0.07]@0.0379792788939272;
GUESS V_b:Eq73 = [0 : 3]@1.61027057225416;
GUESS theta:QS_1 = [0 : 1]@0.404895683485198;


@ z;
@ lpsi;
@ psi;
//@ psi_lead;

@ omega;

USING (C_lag = 1, C_llag = 1, C = 1): @Lambda:Lambda;
USING (C_lag = 1, C = 1): @R:euler;

USING (I = 1): @ f, fp:fp, Iratio:Iratio;
@ Q(1):Qinv;

@ ER_k:SpreadStSt;



//@ Z:R_k = [0.0000000001 : 1000]; // we don't know about R_k
@ R_k(2):R_k2;
@ R_k(1):ER_k;
//@ ER_k;
@ spread;

@ K:mpc = [0.0000000001 : 1000];

@ I:K;
@ Y;

@ D_g;
@ Gbar:GStSt;
@ T, G:G;
								    
@ W;
// need I
@ C:ARC;
@ C_lag;
@ C_llag;
		      
@ s(1):macl1, s(2):macl2, S:S;

// need Q, S, s, R, 
@ N:LRstst, xi:N1, n(2):N2, n(1):N, D:D, D_h:D_h;

@ V_d;
@ R_b:V_b;
//@ mu(1):mu_1, mu(2):mu_2;
@ V_s;
//@ V_s:mu_1a;


//@ phi(1):QS_1;
//@ theta:phi_1;
//@ thetabar;
//@ phi(2):phi_2;


//@ theta:QS_1 = [0: 1];
@ thetabar:theta;
//@ phi(1):phi_1;
//@ phi(2):phi_2;

@ lam(1):lam_1;
@ lam(2):lam_2;
@ lambdabar;

@ b(1):b_i;
@ b(2):b_n;

@ phi;
@ spreadInter;
