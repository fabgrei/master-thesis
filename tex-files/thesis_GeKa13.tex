\section{Adding the New Keynesian structure}
\label{sec:new-keynesian}

This section describes the first New Keynesian block of the model. The
structure is based on \cite{gertler2013qe}. 

The consumption and capital good of the economy is a composite good,
\begin{equation*}
  Y_t = \Bigl( \int_0^1 Y_{ft}^\frac{\epsSubst-1}{\epsSubst} \dd f \Bigr)^\frac{\epsSubst}{\epsSubst-1}.
\end{equation*}
The aggregator implies \ac{ces} across the (differentiated) product
varieties $f \in [0,1]$. Each of them is produced by a \emph{single}
retailer $f$ (who, thus, has some market power). They are subject to
staggered price setting: only a fraction $(1-\gamma)$ can change
prices in every period. The differentiated goods are ``produced'' 
(that is, they get a label) by retailers from a \emph{single intermediate
  good}. The intermediate good is produced by competitive firms with a
Cobb-Douglas aggregate production function
\begin{equation*}
  Y_{m,t} = A_{t}K_{t}^{\alpha}L_{t}^{\alpha}.
\end{equation*}

The competitive intermediate goods producers are described first. 
The retailers take the demand function for their
goods as given. So these demand functions are derived before their
optimality conditions are derived. Subsequently, the law of motion of the
price level is discussed and the link between output, intermediate
good production and price dispersion is given.
% This appendix provides the derivations of the optimality conditions
% for the monopolistically competitive retail firms. These retail firms
% buy the intermediate goods at prices $P_m$ and resell them for the
% optimally chosen price $P^*_m$. Only a fraction of firms is allowed to
% change prices in each period (\emph{Calvo pricing}). For their
% optimization problem they take into account the consumers demand
% functions for the differentiated goods.

\paragraph{The intermediate goods producers} The intermediate goods
producers pay the marginal products on the factor markets 

they take out loans from banks

purchase investment goods from investment goods producer
\checkthis{MISSING}
MPC
MPL

\paragraph{The demand function for good $f$.}
The final output is a composite good. Each (differentiated) component $f \in [0,1]$ is produced by the retail firm with the same
index $f$. The components form a \ac{ces} aggregate
\begin{equation}
  \label{eq:CES}
  Y_t = \Bigl( \int_0^1 Y_{ft}^\frac{\epsSubst-1}{\epsSubst} \dd f \Bigr)^\frac{\epsSubst}{\epsSubst-1}.
\end{equation}
Given the output level $\bar Y_t$, the demand for the components
$Y_{ft}$ is such that costs are minimized. The problem is the following: 

\begin{equation*}
\begin{aligned}
 & \min_{(Y_f: f \in [0,1])} \int_0^1 P_{ft} Y_{ft} \dd f  \\
 & \text{s.t. } \bar Y_t = \Bigl( \int_0^1 Y_{ft}^\frac{\epsSubst-1}{\epsSubst} \dd f \Bigr)^\frac{\epsSubst}{\epsSubst-1}
 \end{aligned}
\end{equation*}
As shown in \citet[appendix 3.1]{gali2009monetary} the first order
necessary conditons yield the demand functions
\begin{equation}
\label{eq:demand}
Y_{ft} = \left(\frac{P_{ft}}{P_t}\right)^{-\epsSubst} \bar Y_t.
\end{equation}
% \intertext{The first order necessary conditions for an optimal
%                                   solution are}
% P_{ft} &= \lambda \Bigl( \int_0^1 C_{ft}^\frac{\epsSubst-1}{\epsSubst}
%          \dd f \Bigr)^{\frac{\epsSubst}{\epsSubst-1}-1}
%          C_f^{-\frac{1}{\epsSubst}} \\
% &=  \lambda \bar C_t^{\frac{1}{\epsSubst}}
%          C_f^{-\frac{1}{\epsSubst}}
% \intertext{The second equality holds since
%   $\frac{\epsSubst}{\epsSubst-1}-1 = \frac{\epsSubst}{\epsSubst -
%   1}\frac{1}{\epsSubst}$. For two goods $f,g$ this implies}
% C_{ft} $= C_{gt} \left(\frac{P_{ft}}{P_{gt}}\right)^{-\epsSubst}

\paragraph{Optimal price setting of retailer $f$.}

The retail firms have market power, they can choose prices. However, they
are subject to staggered price setting. Only a fraction $(1 - \gamma)$
of firms can change their prices every period. Assume firms last
changed their prices in period~$t$ and consider their problem in
period $t+i$: Firm~$f$ buys intermediate goods $Y_{f,t+i}$ at price
$P_{m,t+i}$ and transforms them into final goods one-for-one. These
are re-sold at price $\frac{P^*_{ft}}{P_{t+i}}$. The probability of
not being able to change the price until period $t+i$ is
$\gamma^i$. Thus, the firms' price setting problem is given by
\begin{equation*}
  \max_{P_{ft}} \E_t \sum_{i=0}^\infty \gamma^i \Lambda_{t,t+i}
  \Bigl(\frac{P_{ft}}{P_{t+i}} - P_{m,t+i}\Bigr) Y_{f,t+i}
\end{equation*}
which leads to the following first order necessary condition
\begin{equation*}
\E_t \sum_{i=0}^\infty \gamma^i \Lambda_{t,t+i} \Bigl(
     \frac{P_{ft}^*}{P_{t+i}} - \mu P_{m,t+i} \Bigr) Y_{f,t+i} = 0.
\end{equation*}
\checkthis{INTERPRET THIS EQUATION.} In order to solve the model
numerically, this equation has to be rewritten in a recursive
form. This is done in the appendix~\ref{app:retailer}, where all the
derivations are found, too.


\paragraph{The law of motion for inflation} Since there is a continuum
of retail firms, the law of large numbers holds for the
economy. Tomorrow's price level will be a weighted average of today's
prices (for the firms who cannot change their prices) and the optimal
prices tomorrow (chosen by the firms who \emph{can} choose. The price
index $P_t$ is given by
\begin{align*}
  P_t^{1 - \epsSubst} &= \int_0^1 (P_{ft})^{1 - \epsSubst} \dd f \\
                      &= (1-\gamma) (P^*_{ft})^{1-\epsSubst} + \gamma
        (P_{t-1})^{1-\epsSubst} \\
\intertext{Defining inflation $\Pi_t := P_t / P_{t-1}$ one gets to the
  law of motion}
\iff \Pi_t^{1 - \epsSubst} & = (1 - \gamma) \left(
                            \frac{P^*_{f,t-1}}{P_{t-1}} \right)^{1
                            -\epsSubst} + \gamma \\
                      &= (1 - \gamma) \cdot \Pstar_{t-1}^{1
                            -\epsSubst} + \gamma \numberthis \label{eq:lom-inflation}
\end{align*}

\paragraph{Total output, intermediate good and price dispersion} The
total output $Y_t$ and the production of the intermediate good
$Y_{mt}$ are linked my a measure of price dispersion. $Y_{mt}$ is the
input for all differentiated products $Y_{ft}$, where the production
function is the identity. Thus,
\begin{equation*}
  Y_{mt} = \int_0^1 Y_{ft} \dd f
\end{equation*} 
Use the demand for each variety \eqref{eq:demand} to get
\begin{equation}
  \label{eq:Ym-Y-disp}
  Y_{m,t} = \int_0^1 \left( \frac{P_{ft}}{P_{t}}
  \right)^{-\epsSubst} Y_{t} \dd f = Y_t \underbrace{\int_0^1 \left( \frac{P_{ft}}{P_{t}}
    \right)^{-\epsSubst} \dd f}_{\disp_t}
\end{equation}
The integral $\disp_t$ is a measure of price dispersion. If there is
no dispersion, $\disp = 1$, then the retail production will sum up to
intermediary production. If there is dispersion, $\disp > 1$, some of
the intermediary production will be lost. Total consumption will be
lower than the production. Resources are wasted.  In appendix
\ref{app:disp} it is shown that the price dispersion can be written as
%
\begin{equation*}
 \disp_t = \gamma  \Pi_{t}^{\epsSubst} \cdot \disp_{t-1} + (1 -
          \gamma) \cdot \Pstar_t^{-\epsSubst}.
\end{equation*}

\subsection{Conventional monetary policy}

\checkthis{Missing}

\section{Analysis}


\part{Appendix}

\section{The New Keynesian Part --- the recursive formulation}

This appendix describes the New Keynesian block of this model. It
describes the layers of the production process and provides
derivations of the optimality conditions of the various agents.

\subsection{Optimal price setting of retailer $f$.}
\label{app:retailer}
A fraction $(1 - \gamma)$
of retailers can change their prices every period. The firms' price setting problem is given by
\begin{align*}
    & \max_{P_{ft}} \E_t \sum_{i=0}^\infty \gamma^i \Lambda_{t,t+i}
  \Bigl(\frac{P_{ft}}{P_{t+i}} - P_{m,t+i}\Bigr) Y_{f,t+i} \\
\iff   & \max_{P_{ft}} \E_t \sum_{i=0}^\infty \gamma^i \Lambda_{t,t+i}
   \Bigl(\frac{P_{ft}}{P_{t+i}} - P_{m,t+i}\Bigr) \underbrace{\Bigl(
     \frac{P_{ft}}{P_{t+i}} \Bigr)^{-\epsSubst}
         Y_{t+i}}_{Y_{f,t+i}}. \numberthis \label{eq:4}\\
\iff   & \max_{P_{ft}} \E_t \sum_{i=0}^\infty \gamma^i \Lambda_{t,t+i}
   \left( \Bigl( \frac{P_{ft}}{P_{t+i}} \Bigr)^{1-\epsSubst} - P_{m,t+i} \Bigl(
     \frac{P_{ft}}{P_{t+i}} \Bigr)^{-\epsSubst} \right)
         Y_{t+i}
 \intertext{where \eqref{eq:4} holds since the final goods producers take the
 demand function~\eqref{eq:demand} for good $f$ as given. The first
         order necessary conditions for optimal prices are given by}
  0 &= \E_t  \sum_{i=0}^\infty \gamma^i
      \Lambda_{t,t+i} \Bigl( (1-\epsSubst) \Bigl( \frac{P^*_{ft}}{P_{t+i}}
      \Bigr)^{-\epsSubst} \frac{1}{P_{t+i}} - (-\epsSubst) P_{m,t+i} \Bigl(
     \frac{P^*_{ft}}{P_{t+i}} \Bigr)^{-\epsSubst - 1} \frac{1}{P_{t+i}}
\Bigr) Y_{t+i} \\
 &= \frac{1 - \epsSubst}{P^*_{ft}}\E_t \sum_{i=0}^\infty \gamma^i
   \Lambda_{t,t+i} \Bigl( \Bigl( 
     \cola{\frac{P_{ft}^*}{P_{t+i}}} \Bigr)^{1-\epsSubst}-  \underbrace{\frac{-\epsSubst}{1-
  \epsSubst}}_{=: \mu}P_{m,t+i} \Bigl(\cola{ \frac{P^*_{ft}}{P_{t+i}}
      }\Bigr)^{-\epsSubst}\Bigr) Y_{t+i}, \\
 % &= \E_t \sum_{i=0}^\infty \gamma^i \Lambda_{t,t+i} \Bigl(
 %     \frac{P_t^*}{P_{t+i}} - \mu P_{m,t+i} \Bigr) Y_{f,t+i} \\
  & = \underbrace{\E_t \sum_{i=0}^\infty \gamma^i \Lambda_{t,t+i}
    \Bigl( \frac{P^*_{ft}}{P_{t+i}}
      \Bigr)^{1-\epsSubst} Y_{t+i}}_{=:
    \PrValA_{t}} - \underbrace{\E_t \sum_{i=0}^\infty \gamma^i \Lambda_{t,t+i} \mu P_{m,t+i} \Bigl( \frac{P^*_{ft}}{P_{t+i}}
      \Bigr)^{-\epsSubst} Y_{t+i}}_{=: \PrValB_{t}} \numberthis \label{eq:opt-price}
\intertext{In order to solve the model numerically, the equations have
    to be rewritten in recursive form. This is done separately for the
    two infinite series\footnote{\texttt{Typewriter} font is used for
    auxiliary variables that show up in the computer code.}.}
%
\PrValA_{t} &= \Lambda_{t,t}\Bigl( \underbrace{\frac{P^*_{ft}}{P_{t}}}_{\Pstar_t}
      \Bigr)^{1 - \epsSubst} Y_{t} + \sum_{i=1}^\infty \gamma^i \Lambda_{t,t+1} \Lambda_{t+1,t+i} \Bigl( \frac{P^*_{ft}}{P_{t+i}}
      \Bigr)^{1-\epsSubst} Y_{t+i} \\
  &= \Pstar_t^{1 - \epsSubst} Y_{t} +\gamma \Lambda_{t,t+1} \sum_{i=0}^\infty \gamma^i \Lambda_{t+1,t+1+i}\Bigl( \frac{P^*_{ft}}{\cola{P^*_{f,t+1}}}\frac{\cola{P^*_{f,t+1}}}{P_{t+1+i}}
      \Bigr)^{1 - \epsSubst}  Y_{t+1+i} \\
  &= \Pstar_t^{1 - \epsSubst} Y_{t} +\gamma \Lambda_{t,t+1} \Bigl(
    \frac{P^*_{ft}}{P^*_{f,t+1}} \Bigr)^{1-\epsSubst} \PrValA_{t+1}\\
&= \Pstar_t^{1 - \epsSubst} Y_{t} +\gamma \Lambda_{t,t+1} \Bigl(
    \frac{\Pstar_t}{\Pstar_{t+1}}\frac{1}{\Pi_{t+1}}
  \Bigr)^{1-\epsSubst} \PrValA_{t+1}\numberthis \label{eq:PrVal1} \\
%
\PrValB_t &= \mu \Pstar_t^{-\epsSubst} P_{m,t} Y_{t} +
            \sum_{i=1}^\infty \gamma^i \Lambda_{t,t+1}
            \Lambda_{t+1,t+i} \mu \Bigl( \frac{P^*_{ft}}{P_{t+i}}
      \Bigr)^{-\epsSubst} P_{m,t+i} Y_{f,t+i} \\
&= \mu \Pstar_t^{-\epsSubst} P_{m,t} Y_{t} + \Lambda_{t,t+1} \gamma
  \sum_{i=0}^\infty \gamma^i \Lambda_{t+1,t+1+i} \Bigl( \frac{P^*_{f,t}}{\cola{P^*_{f,t+1}}} \frac{\cola{P^*_{f,t+1}}}{P_{t+1+i}}
      \Bigr)^{-\epsSubst} \mu P_{m,t+1+i} Y_{f,t+1+i} \\
&= \mu \Pstar_t^{-\epsSubst} P_{m,t} Y_{t} + \Lambda_{t,t+1} \gamma  \Bigl(
    \frac{\Pstar_t}{\Pstar_{t+1}}\frac{1}{\Pi_{t+1}}
  \Bigr)^{-\epsSubst} \PrValB_{t+1} \numberthis \label{eq:PrVal2}.
% \intertext{Combining the two expressions we get back the original equation}
% 0 &= P_t^* \vn{PV}_{1,t} - \vn{PV}_{2,t} \\
%   &= 
\end{align*}
We have to use a more general definition for the stochastic discount factor $\Lambda$ than is given in the paper\footnote{We use that $\Lambda_{t,s} = \beta^{s-b} {u_{C_s}}/{u_{C_t}}$ for $s \geq t$. \citet{gertler2013qe} only provide a definiton for $s = t+1$.}.

\subsection{Price dispersion}
\label{app:disp}
The 
total output $Y_t$ and the production of the intermediate good
$Y_{mt}$ are linked my a measure of price dispersion. Recall equation
\eqref{eq:Ym-Y-disp}: 
\begin{equation*}
  Y_{m,t} = Y_t \underbrace{\int_0^1 \left( \frac{P_{ft}}{P_{t}}
    \right)^{-\epsSubst} \dd f}_{\disp_t}
\end{equation*}
 
%
The integral $\disp_t$ is a measure of price dispersion. Order the
retail firms such that all firms who where not allowed to change their
price are in $[0, \gamma]$ and those who re-optimized are in $[\gamma,
1]$. Then split up integral.
\begin{align*}
  \disp_{t} &=  \int_{0}^\gamma \left( \frac{P_{ft}}{P_{t}}
          \right)^{-\epsSubst} \dd f + \int_\gamma^1 \left( \frac{P_{ft}}{P_{t}}
          \right)^{-\epsSubst} \dd f \\
 &=  \int_{0}^\gamma \left( \frac{P_{f,t-1}}{P_{t}}
          \right)^{-\epsSubst} \dd f + \int_\gamma^1 \left( \frac{P^*_{ft}}{P_{t}}
          \right)^{-\epsSubst} \dd f \\
        % &= \gamma  \\left( \frac{P_{t-1}}{P_{t}}
        %   \right)^{-\epsSubst} + (1 - \gamma)  \left( \frac{P^*_{ft}}{P_{t}}
        %   \right)^{-\epsSubst} \\
\intertext{Since the fraction of firms that is allowed to change
  prices is randomly (independently) drawn the distribution in each
  group is unchanged. The fraction $\gamma$ has to stick with their
  price. The others will all choose the optimal price $P_{ft}^{*}$.}
        &= \gamma  \left( \frac{\cola{P_{t-1}}}{P_{t}}
          \right)^{-\epsSubst}\int_{0}^1 \left( \frac{P_{f,t-1}}{\cola{P_{t-1}}}
          \right)^{-\epsSubst} \dd f + (1 - \gamma) \cdot \Pstar_t^{-\epsSubst} \\
        &= \gamma  \Pi_{t}^{\epsSubst} \cdot \disp_{t-1} + (1 -
          \gamma) \cdot \Pstar_t^{-\epsSubst}. \numberthis \label{eq:disp}
\end{align*}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
