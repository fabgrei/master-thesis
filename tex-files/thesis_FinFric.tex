


\section{Financial frictions in the literature}
\label{sec:literature}

Before the Great Recession, most standard macroeconomic models\footnote{That is, \acf{dsge} models.} did not include financial markets explicitly. This relied on the assumption that financial markets are complete\footnote{Completeness of financial markets means there is perfect information and perfect enforcement of debt contracts; the assets are state-contingent (the pay-off depends on the state of the world) and assets can be traded in each possible state.} and \emph{frictionless}.

These markets can be described as markets with perfect trust across agents (lying is not possible, due to perfect information). Moreover, wealth (debt level and asset holdings) does not affect whether an agent will be granted credit. That is because contracts are perfectly enforceable: lenders cannot default on their debt.
%That is, debt markets are complete, there is perfect information in the economy, and debt contracts are perfectly enforceable. That means, lenders are perfectly aware of how borrowers use their loans. Borrowers can neither lie about their effort, nor divert funds.
That is why trust and creditworthiness play no role and ``funds are liquid and can flow to the most profitable project or to the person who values the funds most'' \citep[p.1]{brunnermeier2012survey}. This was in constrast to common knowledge: At least since the Great Depression in the 1930s it had been known that financial markets are \emph{not} frictionless. They might propagate, amplify or even generate shocks to the real economy. In the decades prior to 2007 these frictions seemed small, so most models abstracted from them. However, macroeconomic models with financial frictions have been around at least since \cite{bernanke1989agency}.

This section provides a selective overview of the macroeconomic literature with financial frictions. It will discuss some empirical findings, the classical contributions and the most closely related articles of the  recent literature. 



\subsection{Empirical evidence on financial frictions}
\label{sec:empir-lit}
This section should motivate the theoretical work below by pointing at related empirical work that has been conducted recently. \cite{adrian2012which} show that the Great Recession lead to a drop in loans \emph{to the corporate sector} in the US---both on an aggregate and on a firm level.
% (\checkthis{add an updated plot of macro data})
On the other hand, financing through corporate bonds increased. It almost made up for the decrease in credit. However, a large fraction of the economy is not captured by the data: Smaller---non-corporate---firms have no access to bond finance, so they might have suffered more from decreasing credit. In the firm-level data, \cite{adrian2012which} find that both the number and the volume of loans decreased during the crisis, while increasing for bonds.  What is more, external finance has become more expensive. \citet[section 3.2.1]{adrian2012which} show that the cost of new debt rose sharply during the crisis---from 99 \acp{bp} in Q2:2007 to $403$\acp{bp} in Q2:2009.

They show that changes in banks' loans are debt financed: changes in debt and assets are almost perfectly correlated. On the other hand, equity is ``sticky''---the correlation to assets is almost zero. Thus, if banks change their leverage ratio
\begin{equation*}
  \frac{\text{assets (loans)}}{\text{net worth}},
\end{equation*}
they do this through a change in assets. This implies that bank leverage co-moves with the business cycle.

\cite{schularick2012creditbooms} analyze how credit and money aggregates behave in the years before and after financial crises. They use data of  fourteen countries and 140 years. They find that lagged credit growth has some predictive power for predicting financial crises in logit regressions---it does better than money supply. The difference between money and credit growth is strong only after \ac{ww2}, when money and credit growth started to decouple. \cite{schularick2012creditbooms} also find an increase in inflation and money growth rates after \ac{ww2}. In particular, after financial crises hit, inflation went slightly up after 1945, while inflation had become negative before 1938. This suggests that the policy responses to financial crises have been more expansionary post \ac{ww2}.

These findings suggest that it is important to include imperfect financial markets to macroeconomic models. 

\subsection{Financial frictions in macroeconomic models}
\label{sec:theor-lit}
In order to introduce financial frictions into macroeconomic models, 
%Financial frictions can be introduced to  macroeconomic models by giving up the assumptions of perfect financial markets. 
agents can be given the opportunity to lie (misreport private information) or steal (break debt contracts). A different way is to set an exogenous debt limit for borrowers. These assumptions will have two implications: $(i)$ external funds (debt) are  more expensive than internal funds (equity), and $(ii)$ wealth of a lender plays a role: the more net worth she has, the more debt she may obtain.

This thesis will analyze the model by \citet{gertler2011handbook} who explicitly model banks as financial intermediaries. Their predecessors did not explicitly mention banks when they analyzed financial frictions, but they used similar mechanisms.

\paragraph{Costly state verification}
%In order to introduce financial frictions into a model, one has to give up the assumption of perfect financial markets. The early literature \citep[e.g.][]{bernanke1989agency,carlstrom1997agency} assumed that the lender cannot observe the actual return of the borrower only at some monitoring costs. The borrower has an incentive not to reveal the actual return, in order to increase its own rent. In equilibrium, the borrower will always repay the debt, unless its profits are too small. In that case the borrower monitors the actual returns of the entrepreneur, just to notice that the profits fall short of the debt. The monitoring costs are a source of inefficiency for the economy. 
 The models\footnote{Even though the financial frictions are modelled the same way, the contexts differ. While the analysis by \cite{bernanke1989agency} is set in an 
\ac{olg} \acreset{olg}
 framework,  \cite{carlstrom1997agency} do quantitative analysis in an 
\ac{rbc} \acreset{rbc}
framework with an infinitely lived representative agent and \cite{bernanke1999financial} add the New Keynesian structure with Inflation and monetary policy.} 
by \cite{bernanke1989agency}, \cite{carlstrom1997agency} and \cite{bernanke1999financial} place the  friction with entrepreneurs, who transform investment to capital goods through ``projects''. These are financed both internally (with their own net worth) and externally (loans from the lenders). They are given the opportunity to hide their true returns and pay out less than agreed. The principal (the household as lender) can monitor the entrepreneur at a cost, which is dead weight loss. Similarly to the newer models, the entrepreneurs' balance sheets play a key role.

The friction arises because there is asymmetric information: The entrepreneurs' projects have stochastic outcomes, which are private information of the entrepreneurs. The entrepreneurs can report a lower outcome (cutting down the lenders return) and consume the difference. In order to avoid misreporting, the lenders have the possibility to monitor the entrepreneurs at a costs (``costly state verification''). Expected agency costs (i.e.\ monitoring costs) are decreasing in the level of net worth. That is because with higher net worth the lender's stake in the project gets smaller, and she has less of an incentive to pay the monitoring costs.

Once net worth is sufficiently high (the project can be financed purely from net worth\footnote{\citet{bernanke1989agency} talk about ``full-collateralization''}) there is no more need to monitor, and thus no more agency costs at all. This can be ruled out in a quantitative analysis, for example by a finite expected lifetime of borrowers. In that case external finance is needed and there are agency costs. That is where the \emph{financial accelerator} kicks in: in bad times the weaker balance sheets reduce investment demand---which amplifies the downturn.

\cite{carlstrom1997agency} show that financial frictions produce hump-shaped impulse responses to productivity shocks in an otherwise standard \ac{rbc} model. They argue that financial frictions can be used to endogenize capital adjustment costs. \cite{bernanke1999financial} use both financial frictions and capital adjustment costs to get stronger feedback effects. After a negative productivity shock, net worth falls. Subsequently asset demand and, thus, asset prices fall. This further depresses the net worth (which is related to assets via the balance sheet relation, see figure~\ref{fig:bal-sheet-inv} below).

\paragraph{Collateral constraints and exogenous borrowing limits} The models by \citet{kiyotaki1997credit, kiyotaki2012liquidity}, \citet{gertler2011unconventional} and \citet{gertler2011handbook} share the idea that lenders cannot force borrowers to repay their debt---unless debt is secured. If the debt level of a borrower is high enough, she might have the incentive to break the debt contract and not to repay the loan. This gives rise to an \emph{{icc}} which puts an upper bound on debt. This ensures that the debtor will always repay. Inspired by the events of the Great Recession, \cite{gertler2011unconventional} and \cite{gertler2011handbook} explicitly introduce banks as financial intermediaries in their models. As opposed to the earlier literature, the banks are considered as the \emph{borrowers} who receive funds from depositors (or other banks). In this setup banks have the possibility to steal a fraction of these assets. They will only receive funds as long as the expected value of future profits (the value of the bank) exceeds the funds that can be diverted---that is, as long as they have no incentive to divert funds.

In order to achieve borrowing and lending in equilibrium, a model must feature heterogeneity of agents. One way to achieve this is to assume that some agents are less patient than others. However, there needs to be a bound on borrowing, to prevent the impatient agents to borrow infinite amounts. While \cite{kiyotaki1997credit} introduce an endogenous bound through the collateral constraint, \cite{eggertsson2012deleveraging} set an exogenous debt limit. They show how a drop in the debt limit leads to higher interest rates and a debt-deflation mechanism.  \cite{eggertsson2014secular} extend this idea in an \ac{olg} setup. They argue that decreasing debt limits might push economies into ``secular stagnation''---that is, extended periods with zero interest rates (but a negative natural interest rate, so that the zero lower bound binds). The channels at work are similar in all models mentioned.

\paragraph{} The literature on financial frictions is much broader. It is surveyed in \citet{brunnermeier2012survey}. Other approaches include the work by \citet{geanakoplos2008anxious, geanakoplos2015innovation} and \citet{geanakoplos2010leverage}, who analyze the financial crisis in a microeconomic framework.

% % \section{Debt and Leverage}
% % \label{sec:debt-leverage}

% % An important characteristic on the economy is debt and leverage. By
% % households, firms, banks and the financial secto
% % \citet{eggertsson2012deleveraging}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis2"
%%% End:
