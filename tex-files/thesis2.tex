 \documentclass[11pt,a4paper,x11names,dvipsnames]{article}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}
\def\w{$\omega$}
 
\usepackage{geometry}
 \geometry{
 left=40mm,
 right=25mm,
 top=25mm,
 bottom=30mm,
 }
%% \usepackage{titlesec}
%% \newcommand{\sectionbreak}{\clearpage}

 \usepackage{beamerarticle}


 \input{headerFiles/header}
 \input{headerFiles/math-def}
 \input{headerFiles/tikz-def}
 \input{headerFiles/acronyms}

 \usepackage{tikz}
%\usepackage[left]{showlabels}

\usepackage{array, booktabs}
\usepackage{colortbl}
\usepackage{ctable}
\usepackage{caption}

%%% Titlepage, affidavit
\usepackage{graphicx}
\usepackage{titling} %\thetitle, \theauthor and \thedate 
\usepackage{lastpage} %\LastPage
\newcommand{\studID}{1050810}
\newcommand{\thesupervisor}{Michael Reiter}

%opening

\title{Financial frictions and the interbank market}
  % \title{The minor role of the interbank market in a
  %   macroeconomic model with financial frictions} 

\author{Fabian Greimel}
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}





 
\input{headerFiles/cover_affidavit_IHS}
%needs titling, lastpage, graphicx packages and studID, thesupervisor commands


\clearpage


\tableofcontents

\clearpage
%\vspace*{\fill}

\listoffigures
 
\vspace*{\fill}

\listoftables

\vspace*{\fill}

\printacronyms[name=List of Abbreviations]

\vspace*{\fill}

%\input{thesis_abstract}
  


\newpage 
 \input{thesis_intro}
 \input{thesis_FinFric}
 \input{thesis_GeKi11}

%<<this, child="resultsChild.Rnw">>=
%@ 



\section{Numerical analysis and crisis simulation}

In this section I will analyze the model quantitatively. After a
description of the parameter calibration, I will discuss how to
simulate a crisis in this model. The financial crisis is modelled as
an exogenous shock to capital quality. The model features a
\emph{financial accelerator} which amplifies the shock. I fail to
replicate the impulse responses by \citet[figure
2]{gertler2011handbook}. However, I will provide arguments why their
figures might not correspond to the parameterization given in their
paper. I will show that the interbank market is very small relative to
total credit. The interbank friction affects the solution of the model only through the \ac{icc}. Since the interbank market is very small, a change in the degree of interbank friction cannot have an important impact on the economy: \emph{The interbank market plays only a minor role}.

%% As usual in the literature the quantative implications of the model are studied after solving a linear approximation of the model (around the non-stochastic steady state $\vec{x}^*$),
%% \begin{equation*}
%% \vec{x}_{t+1} - \vec{x}^* =  A (\vec{x}_t - \vec{x}^*) + B \vec {\eps}_t,
%% \end{equation*}
%% where $\vec x_t$ is a vector of all variables of the model. The deviations from steady state are, thus, modelled as a vector $\ar(1)$ process with the innovations $\vec \epsilon_t$ being shocks to productivity, capital quality and frictions. The solution is obtained using the toolkit by \citet{reiter2015toolkit}. The used program code is available as a text file.

\subsection{Calibration}




\begin{table}[tbp]
  \centering
  \caption{Baseline parameter choices for the numerical analysis}
%\begin{threeparttable}
\resizebox{\linewidth}{!}{
% latex table generated in R 3.2.0 by xtable 1.7-4 package
% Wed Jun 17 08:55:28 2015
\begin{tabular}{lll}
  \toprule
parameter & value & description \\ 
  \midrule
$ \beta $ & 0.99 & consumers' discount factor \\ 
  $ \gamma $ & 0.5 & habit formation parameter \\ 
  $ \chi $ & 5.584 & relative weight of labour in utility \\ 
  $ \epsilon $ & 0.1 & inverse Frisch labour elasticity \\ 
  $ \alpha $ & 0.33 & capital share \\ 
  $ \delta $ & 0.025 & depreciation rate of the capital stock \\ 
  $ \bar{\omega} $ &  & steady state fraction of save interbank assets (interbank friction) \\ 
  $ {\pi^i} $ & 0.25 & probability of new investment opportunities to arrive \\ 
  $ \bar{\theta} $ & calibrated & steady state fraction of divertable asset (general degree of friction) \\ 
  $ \xi $ & calibrated & transfer to entering bankers \\ 
  $ \sigma $ & 0.972 & Survival  rate  of the bankers \\ 
  $ {\eta_I} $ & 1.5 & Inverse elasticity of net investment to the price of capital \\ 
  Gshare & 0.2 & Steady state proportion of government expenditures \\ 
  $ {\bar G} $ & calibrated & steady state government expenditure \\ 
  $ {\rho_\psi} $ & 0.66 & autoregressive parameter of log capital quality $\psi_t$ \\ 
  $ {\rho_\omega} $ & 0.66 & autoregressive parameter of $\omega_t$ \\ 
  $ {\rho_\theta} $ & 0.66 & autoregressive parameter of $\theta_t$ \\ 
  LR & 4 & steady state leverage ratio \\ 
  SPREAD & 0.0025 & steady state average credit spread \\ 
   \bottomrule
\end{tabular}

}
%\begin{tablenotes}
\flushleft
\footnotesize All parameters are taken from \citet{gertler2011handbook} except the
autoregressive coefficients of the friction parameters.  The
steady state  degree of interbank friction $\bar \omega \in [0,1]$
will be varied in the numerical analysis.
%\end{tablenotes}
%\end{threeparttable}
    \label{tab:para}
\end{table}
I follow \cite{gertler2011handbook} with their parameter choices,
which are given in table~\ref{tab:para}. In the quantitative analysis,
however, I assume
that the degrees of friction $\theta_t$ and $\omega_t$ are time
dependent, following $\ar(1)$ processes,
\begin{align*}
  \theta_t &= (1 - \rho_\theta) \bar \theta + \rho_\theta \theta_{t-1} \\
  \omega_t &= (1 - \rho_\omega) \bar \omega + \rho_\omega \omega_{t-1}.
  \end{align*}
While the steady state degree of general friction $\bar
\theta$ is calibrated, the steady state degree of interbank friction $\bar \omega$ is not. Instead, results are compared for different levels of friction. $\bar \theta$ and $\xi$ are set to hit following targets in steady state:
\begin{enumerate}
\item An average credit spread of 100 \acp{bp} per year. Given this period's island type $h$, the expected return is
\begin{align*}
\E_{h'} R^{hh'}_{k,t+1} &= \sum_{h'} \pi^{h'} \frac{ Z_{t+1} + (1 - \delta) Q_{t+1}^{h'}
          }{Q_t^h}\psi_{t+1}\\
\intertext{where $h'$ is next year's island type. So the average interest rate across all islands is}
\E_t R_{k,t+1} &:= \sum_{h} \pi^h \E_{h'} R^{hh'}_{k,t+1}.
\intertext{Now we can target the average spread (risk premium) to be}
\E_t (R_{k,t+1} - R_t) &\stackrel{\text{cali}}{=} \frac{0.01}{4}
        \end{align*}
since in the model one period corresponds to a quarter.

\item An economy-wide leverage ratio of 4. The leverage ratio is the ratio of the value of total bank loans to total banks' net worth:
\begin{equation*}
\frac{  Q^i_t S^i_t + Q^n_t S^n_t}{N^i_t + N^n_t} \stackrel{\text{cali}}{=} 4.
\end{equation*}
\end{enumerate}







\subsection{Crisis experiment: The financial accelerator}
\label{sec:fin-acc}

One way to trigger a financial crisis in the model is a negative shock
to capital quality $(\psi)$. This shock in capital quality depresses
the value of the loans the banks have in their balance sheets. It is
motivated by the idea that the Great Recession was triggered by a
decline in housing prices. Mass default led to massive write-offs in
banks' balance sheets. Note, however, that the model is very
simplistic. It features neither a housing market nor mortgage-backed securities. 

\paragraph{The financial accelerator}

Figure~\ref{fig:vary_omega_shock_psi} shows the impulse responses to a $5\%$ shock in capital quality. The shock translates one-for-one to capital (which equals total loans) in the first period, and is amplified to lead to a much larger drop in net worth (almost $50\%$).

The mechanism of the ``financial accelerator'' is revealed by the evolution of banks' net worth~\eqref{eq:agg-net}. It is determined by quantities and returns from the previous period $(S_{t-1}, D_{t-1}, R_{t-1})$, the exogenous capital quality $\psi_t$ and asset prices $Q_t^h$.

There is a \emph{direct effect}: net worth decreases by $5\%$ of the previous assets. The direct link of assets and the capital stock is due to market clearing on the credit market, see equation~\eqref{eq:macl}.
 
There is also \emph{price effect}. The lower capital quality drives
down the price of capital $Q_t^h$. This further shrinks the banks' net
worth. Banks have to ``fire-sell'' bank assets in order to satisfy the
balance sheet identity. This is reflected in a drop in  investment. This drop in investment demand further depresses asset prices, and thus investment demand, and so on.

% By the balance sheet identity~\eqref{eq:bal-sheet} the banks' net worth declines, and thus their value~\eqref{eq:bank-value}. This tightens the (already binding) incentive compatibility constraint~\eqref{eq:bank-icc}: the bank is forced to deleverage (``fire-sales'' of assets).
% }
% The fire-sales directly decrease the bank's value~\eqref{eq:linear-guess} because it is linear in assets.
    
% But it also depresses the bank's value indirectly, through lower asset prices and, thus, lower net worth
    
%   there is a force in the other direction: returns go up, and thus  bank's marginal value of assets~\eqref{eq:Vs-gen}.
  
% xand thus the value of the bank
% (through marginal value of 
% \checkthis{shock, net worth and asset prices, firesales, ...}
% As described in section~\ref{sec:banks}, the model  the evolution to banks'  net worth is of central interest because it determines total lending $S_t$ and consequently investment $I_t$. The central equation in this version of the model is the incentive compatibility constraint~\eqref{eq:banks-icc} which ties bank lending to bank net worth.

% As can be seen in figures~\ref{fig:vary_omega_shock_psi} a five percent decrease in
% capital quality translates into a much larger decrease in net banks'
% net worth. That is because a decrease in capital quality lowers the
% price of assets $Q_t$ and thus their total value $Q_t S_t$. From the
% aggregate resource constraint~\eqref{eq:icc} it follows that net worth decreases. The worse capital quality will also lead to negative investment (``fire sales''), further depressing the price of capital.

% It is interesting to compare the magnitudes of the different responses. On impact, the total assets $S_t$ decrease only by $5 \%$ since they are function of today's capital stock (see equation~\eqref{eq:lomK}) which is---besides the capital quality---fixed the period before. The shock reduces $Q_t$, $I_t$ and $N_t$, such that the incentive compatibility constraint gets tighter and lending $S_t$ decreases in the subsequent periods.  
% \checkthis{this is the financial accelerator}

\begin{figure}
\begin{subfigure}{\linewidth}
\centering
%results='hide',
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\linewidth]{figure/article-vary_omega_shock_psi-1} 

}



\end{knitrout}

\caption{More friction (smaller $\omega$) leads to less pronounced
  impulse responses. The response of the expected spread does not
  change a lot.}
\label{fig:vary_omega_shock_psi}
\end{subfigure}\\[1ex]
\begin{subfigure}{\linewidth}
\centering
\resizebox{0.8\linewidth}{!}{  \includegraphics{fig-geki2}}
  \caption{Comparison a perfect $(\omega=1)$ and imperfect
    ($\omega=0$) interbank markets  \cite[a reprint of][figure
    2]{gertler2011handbook}. If there are frictions, the impulse
    responses are stronger. The response to the expected spread
    changes a lot.}
  \label{fig:geki2}
\end{subfigure}
\caption[Comparing the results of the crisis experiment]{Comparing the results of the crisis experiment: Impulse
  responses to a $5\%$ shock in capital quality for different degrees of interbank friction $\omega$.}
\label{fig:test}
\end{figure}



\paragraph{Comparison to \citet[figure 2]{gertler2011handbook}}

\citet{gertler2011handbook} provide a general model of the interbank market in their appendix. They analyze only special cases quantitatively, though. Figure~\ref{fig:geki2} shows their figure~2 which compares the cases of perfect ($\omega=1$) and imperfect ($\omega=0$) interbank markets.

The case of a perfect interbank market is matched pretty well. All impulse responses are essentially equivalent in figure~\ref{fig:vary_omega_shock_psi} and \citet{gertler2011handbook}. However, if the interbank friction increases, my results show that the impulse responses become weaker in magnitude. This contradicts \citet{gertler2011handbook}. The difference is especially stark for the spread, which changes a lot in their figure, but only little in mine.

\begin{table}[tbp]
  \centering
  \caption{Comparing the calibrated parameter $\theta$}
%\begin{threeparttable}

% latex table generated in R 3.2.0 by xtable 1.7-4 package
% Tue Jun 16 16:38:20 2015
\begin{tabular}{rrr}
  \toprule
$\omega$ & $\theta$ & $\theta_{GK11}$ \\ 
  \midrule
0.00 & 0.42 & 0.13 \\ 
  1.00 & 0.38 & 0.38 \\ 
   \bottomrule
\end{tabular}

 
\flushleft
\footnotesize The values from \citet[table 1]{gertler2011handbook} are
denoted by $\theta_{GK11}$, my calibrations by $\theta$. I use my
model with $\omega = 0.998$ for comparison with their model where $\omega=1$.
%\end{tablenotes}
%\end{threeparttable}
    \label{tab:theta}
\end{table}

Table~\ref{tab:theta} shows that \cite{gertler2011handbook} report large differences in the calibrated value of the general degree of friction $\theta$. When going from the highest to the lowest interbank friction, the general friction almost triples. In my calibrations it even decreases slightly. As with the impulse responses, my results are very close for the case where $\omega=1$. Figure~\ref{fig:ststval} shows the U-shaped relationship of the calibrated value and $\omega$.

\paragraph{Higher friction, lower levels}
It might seem counterintuitive that a lower friction leads to stronger
impulse responses in a crisis. However, it is not only the impulse
responses that matter, but also the steady state values. Figure~\ref{fig:ststval} shows how the steady state values of the key variables change with different degrees of steady state interbank friction $\bar \omega$. Higher $\bar \omega$ (lower friction) leads to higher levels of output $Y$, consumption $C$, investment $I$, capital $C$ and labor $L$ in steady state. That is, the friction reduces the efficiency of the economy.

%With symmetric frictions $\bar \omega = 0$ on interbank and deposit
%markets there is no interbank spread. The panel showing the interbank spread (over deposits) 
%\checkthis{discussing spread?}
%\checkthis{add leverage ratio?} 
%economy-wide leverage ratio $\phi_t$ goes down

\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}

{\centering \includegraphics[width=0.9\linewidth]{figure/article-ststval-1} 

}

\caption[Steady state levels of selected variables for different parameterization of ]{Steady state levels of selected variables for different parameterization of $\omega$. All other variables are unchanged. Lower $\omega$ means \emph{more} friction.}\label{fig:ststval}
\end{figure}


\end{knitrout}





\subsection{So small: Providing an upper bound for interbank lending}
\label{sec:role-interbank}

Figure~\ref{fig:ststval} shows that the steady state level of interbank lending is very small relative to total credit. As it turns out, this is due to the construction of the model. This section formally derives an upper bound of interbank lending. Under most sensible parameter choices, interbank lending will be smaller than aggregate investment, which is $\delta K^*$ in steady state.

That is because the interbank market only reflects the differential need for loans on the two types of islands. The need for an interbank market comes from heterogenous liquidity needs. A fraction $\pi^i$ of firms may invest, the remaining firms must not. By contrast, all firms have to roll over the loans for their (depreciated) capital stock $(1-\delta) \cdot K_t$ in every period. The additional liquidity needs on investing islands are comparatively small. In steady state, investment will just keep the aggregate capital stock constant,
\begin{equation*}
  I^* = \delta \cdot K^*.
\end{equation*}
The difference in liquidity needs is relatively small across island types. This can be seen figure~\ref{fig:agg-bal-sheet-proof}.

On the liability side, note that interbank lending and borrowing have to net out on aggregate. That is, total loans have to match deposits $D_t$ and net worth $N_t$:
\begin{equation*}
  Q_t^i S_t^i + Q_t^n S_t^n = N_t  + D_t.
\end{equation*}
This gives rise to the following proposition.

\begin{prop*} \label{prop:small-interbank}
Given the model framework of \cite{gertler2011handbook}, the aggregate
interbank borrowing $B^i_t$ across investing islands is given by
\begin{align*}
 B_t^i &= \pi^n Q_t^n I_t + \Phi 
\shortintertext{where}
\Phi &= \pi^n \underbrace{(Q_t^n - Q_t^i)}_{\geq 0} \bigl( (S^i_t -  I_t)\psi_t - S_t^i\bigr). 
\intertext{With a capital quality $\psi_t$ satisfying}
       \psi_t &\leq {\frac{S^i_t}{S^i_t - I_t}},       
\intertext{which is satisfied in a neighbourhood of the deterministic
       steady state, an upper bound on interbank borrowing $B^i_t$ is
       given by}
 0 &\leq B_t^i \leq \pi^n Q^n_t I_t.
       \numberthis \label{eq:9}
     \intertext{  With the calibration used in their paper, $\pi^n Q^n_t \leq 1$ in
    steady state. Thus, $B^{i*} \leq I^*$.  }
\end{align*}

  \begin{proof}[Idea of the proof. The detailed proof is given in appendix~\ref{app:proof-small-interbank}]
    
    The idea can be
    seen in figure~\ref{fig:agg-bal-sheet-proof}. As pointed out
    before, interbank assets are 0 in aggregate (they are in zero net
    supply). Banks will trade on the interbank market only if their
    net worth and deposits do not suffice to satisfy the demand for
    credit. The interbank lending is  given by 
    \begin{equation*}
    B^i_t = Q_t^i S_t^i -  N_t^i - \pi^i D_t.
  \end{equation*}

  Total assets and total liabilities are equal on both islands. Since
  investment $I_t$ is small, the distribution of assets over islands
  types is roughly the same as $\pi^i : \pi^n$. On the liability side,
  recall that net worth is given by 
    \begin{equation*}
      N_t^h = \pi^h  \Bigl(\bigl(Z_t + (1 - \delta) Q^h_t\bigr)
    \psi_t S_{t-1} - R_{t-1} D_{t-1}\Bigr)
     \end{equation*}
     The asset prices $Q^i_t, Q^n_t$ are of the same magnitude
     on the two island types. That is why the distribution of net
     worth is also close to  $\pi^i : \pi^n$.
     
     Thus, assets and liabilities roughly have the same distribution across
     island types, so the need for interbank lending will be small.
  \end{proof}
\end{prop*}

\begin{figure}[tbp]
  \centering
    \input{fig-agg-bal-sheet2}
    \caption[The size of the interbank market]{The aggregate balance sheet by island
      types. {Identification of the
        \tikz[baseline=-\the\dimexpr\fontdimen22\textfont2\relax ]
        \node[rectangle,preaction={fill=gray, fill opacity=0.3}]
        {interbank market}; as difference between assets and
        liabilities of each island.}}
    \label{fig:agg-bal-sheet-proof}
\end{figure}



\paragraph{In the real world} 




\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}

{\centering \includegraphics[width=0.8\linewidth]{figure/article-interbankdata-1} 

}

\caption[Total bank credit and interbank lending by US commerical banks]{Total bank credit and interbank lending by US commerical banks. The shaded areas denote recessions. Source: FRED}\label{fig:interbankdata}
\end{figure}


\end{knitrout}

On the first glance, the data support a very small interbank
market. Figure~\ref{fig:interbankdata} shows the \emph{balance sheet
  level} of interbank credit and total credit for US commercial
banks. It can be seen that the interbank loans are between $0.7\%$ and $8.6\%$ of total credit. The steady state values of our model show that this ratio is of a similar magnitude: $1.8\%$.

However, it can be seen in the lowest panel of figure~\ref{fig:interbankdata} that the interbank market was much bigger before the crisis than it is now. A macroeconomic model that wants to explain the Great Recession should probably aim at matching the levels before the crisis.

But there is an important aspect missing. These data reflect \emph{long-term} interbank lending, that show up in the banks balance sheets. A big chunk of interbank lending is overnight lending. Banks use this instrument to satisfy their reserve requirements. These credits are usually paid back within a day, so they do not show up in banks balance sheets (which only show the \emph{level} of outstanding credit as parts of assets and borrowing as parts of liabilities). That is why data on overnight trade is hard to get.

\citet[table 1]{Demiralp2004} estimate a \emph{daily volume} of
$\$145$ billion of overnight lending for the first quarter of
1998. That is, withing two days the flows of interbank lending exceed
the balance sheet stocks: on average, around
$58\%$ of the stock was traded \emph{per day}.

A model of the interbank market, that cannot replicate the large
flows, might thus miss important aspects the effects that the
interbank market can have on the economy. If these large daily flow
play an important role, it is not adequate to use a quarterly model
frequency. However, a reduced frequency will, other things equal, also
lead to smaller flows. This is because the interbank flows are bounded
by a multiple of investment. In steady state, investment is given by
$\delta K^*$. The depreciation rate $\delta$ will be smaller, the
shorter the time periods.

\subsection{Why the interbank market does not matter in \cite{gertler2011handbook}}
\label{sec:not-matter}
\begin{figure}
\begin{subfigure}{\linewidth}
\centering
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\linewidth]{figure/article-IRtheta1-1} 

}



\end{knitrout}
\label{fig:IRtheta1}
\caption{$5 \%$ shock to general degree of
  friction $\theta_t$ (higher $\theta_t$ means more friction)}
\end{subfigure}\\[1ex]
\begin{subfigure}{\linewidth}
\centering
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\linewidth]{figure/article-IRomega1-1} 

}



\end{knitrout}
\label{fig:IRomega1}
\caption{$\ensuremath{-5}\% $ shock to degree of interbank friction $\omega_t$ (lower $\omega_t$ means more friction)}
\end{subfigure}
\caption{Impulse responses to shocks to the degrees of friction I.}
\label{fig:IRfric1}
\end{figure}

\begin{figure}
\begin{subfigure}{\linewidth}
\centering
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\linewidth]{figure/article-IRtheta2-1} 

}



\end{knitrout}
\label{fig:IRtheta2}
\caption{$5 \%$ shock to general degree of
  friction $\theta_t$ (higher $\theta_t$ means more friction)}
\end{subfigure}\\[1ex]
\begin{subfigure}{\linewidth}
\centering
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\linewidth]{figure/article-IRomega2-1} 

}



\end{knitrout}
\label{fig:IRomega2}
\caption{$\ensuremath{-5}\% $ shock to degree of interbank friction $\omega_t$ (lower $\omega_t$ means more friction)}
\end{subfigure}
\caption[Impulse responses to shocks to the degrees of friction
II.]{Impulse responses to shocks to the degrees of friction:
  Multipliers, spreads, the asset price. While there is a significant
  response to a shock in $\theta_t$, the response to $\omega_t$ is
  very small.}
\label{fig:IRfric2}
\end{figure}

%% 1 - impulse responses are different in magnitude
%% 2 - how do frictions affect the economy formally
%% 3 - numerical results

We have just seen that the size of the interbank market is very
small. I will now show that that this implies that the friction on the
interbank market is quantitatively irrelevant in the given model by
\cite{gertler2011handbook}.

Let us first analyze how the economy reacts to a change in the degrees of friction. As opposed to \cite{gertler2011handbook} I assume that the degrees of friction are time-varying. They follow $\ar(1)$ processes with means at the steady state levels $(\bar \theta, \bar \omega)$. A negative shock to $\omega$ (higher friction) can be interpreted as sudden drop in trust among banks. This is motivated by the freezing of the interbank market after the crash of Lehman brothers.

% As described at the beginning of section~\ref{sec:role-interbank} the interbank market is small relative to total output, so small shocks will be quantitatively negligible. That is why I will analyse the effects of ``large'' shocks\footnote{This approach is flawed. We
%   analyse linear approximation of a non-linear model. This
%   approximation is valid only in a small neighbourhood of the
%   non-stochastic steady state. That is, for ``small'' shocks. The
%   analysis would be more sensible with non-linear approximations, but
%   this requires a much greater computational effort.} of $\epsilon_{\omega,t} =
% 0.5$. That is, from one period to the next, $50\%$ of interbank assets
% of a bank become divertible. The impulse responses are shown in
% figure~\checkthis{?}. 

Figure~\ref{fig:IRfric1} shows selected impulse responses to shocks to the degrees of friction $(\theta_t, \omega_t)$. While the shape of the responses are very similar in panels~~\ref{fig:IRfric1}(a) ($\theta_t$) and~\ref{fig:IRfric1}(b) ($\omega_t$) the magnitudes of the shocks vary a lot. While a $5\%$ to the general friction leads to significant responses of output and total net worth, the responses to a shock to $\omega_t$ are negligible. The mechanism behind is very similar to the financial accelerator of section~\ref{sec:fin-acc}. There is no direct effect on the capital stock, though. The banks are forced to sell assets (disinvest) because more friction makes the \ac{icc} bind more tightly. This reduces the asset prices, banks sell even more assets, and so on. 

Let us now analyze the effect of the friction analytically. The degrees of friction enter the model through the banks' \ac{icc}. The aggregate \acp{icc} for each island type $h \in \{ i,n \} $ at the end of period $t$ are given by
    \begin{equation}
\label{eq:agg-icc2}
      \begin{aligned}
        V_t(S^i_t, B^i_t, \pi^i D_t) &\geq
              {\theta} (\underbrace{Q_t^i
          S^i_t}_{\approx 1.5} - {\omega}
                                       \underbrace{B^h_t}_{\approx 0.1}), \\
       V_t(S^n_t, B^n_t, \pi^n D_t) &\geq
              {\theta} (\underbrace{Q_t^n
          S^n_t}_{\approx 4.5} + {\omega}
                                       \underbrace{B^n_t}_{\approx 0.1}).
                                     \end{aligned}
               \end{equation}
The magnitudes depend on the parameterization. They are taken from figure~\ref{fig:ststval}. The degrees of friction affect the tightness of the \acp{icc}, thus the interest spreads in the economy (see section~\ref{sec:spreads}). The equations above reveal that changes in the interbank friction cannot have a big impact on spreads. Even going from one extreme to the other ($\omega = 0$ to $\omega = 1$) reduces the right hand side of~\eqref{eq:agg-icc2} by only about $2\%$ on non-investing islands and $6\%$ on investing islands.

 
% A change in $\theta_t$ has a much greater impact on the \ac{icc} than a change in $\omega_t$. That is because $\omega_t$ is multiplied by two factors smaller than one.

Figure~\ref{fig:IRfric2} shows impulse responses to shocks to the friction parameters for a different set of variables. Among these variables are the Lagrange multipliers $\lambda^h_t$ corresponding to the above constraints~\eqref{eq:agg-icc2}. The multipliers indicate how strong the contraint is binding. The plots confirm that there is only a very small reaction to a shock in the degree of interbank friction $\omega_t$. By contrast, the reaction to a change in the general friction $\theta_t$ yields a much stronger response.

The plots also show  that the impulse responses of prices (that is, asset prices and interest spreads) share the same pattern: they are almost unaffected by a change in the interbank friction.

 % \subsection{Why the interbank friction does not matter}

% show aggregate \ac{icc} for both types, argue that interbank lending is small, doesn't play a role
% XXX




% \paragraph{How to make the interbank market important}
% If the friction parameters made a difference it would make investment more expensive, and thus lower it. But if there are capital adjustment costs around, changes in investment is already expensive. So, lowering the adjustment cost parameter increases the responses. This is shown in \checkthis{figure?}.




%\input{thesis_qe}
%\input{thesis_GeKa13}
\input{thesis_backmatter}

\bibliographystyle{agsm}
\bibliography{headerFiles/mabib}

%\clearpage

\begin{appendix}
   \input{thesis_GeKi11_app}
   \input{thesis_app_proof}
  % % \input{thesis_appendix}
  % % \input{thesis_GeKi11_app3}
\end{appendix}


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
