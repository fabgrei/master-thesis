% Created by tikzDevice version 0.8.1 on 2015-06-29 08:04:11
% !TEX encoding = UTF-8 Unicode
\documentclass[mathserif,x11names,dvipsnames,red]{beamer}
\nonstopmode

\usepackage{tikz}

\usepackage[active,tightpage,psfixbb]{preview}

\PreviewEnvironment{pgfpicture}

\setlength\PreviewBorder{0pt}

\newcommand{\SweaveOpts}[1]{}  % do not interfere with LaTeX
\newcommand{\SweaveInput}[1]{} % because they are not real TeX commands
\newcommand{\Sexpr}[1]{}       % will only be parsed by R


\def\w{$\omega$}

%\usefonttheme{serif}
\usepackage{etex}
\usepackage{appendixnumberbeamer}
\newcommand{\backupbegin}{
   \newcounter{framenumberappendix}
   \setcounter{framenumberappendix}{\value{framenumber}}
}
\newcommand{\backupend}{  \addtocounter{framenumberappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumberappendix}} 
}

\setbeameroption{hide notes} % Only slides
%\setbeameroption{show notes} % show notes 
%\setbeameroption{show only notes} % Only notes

\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

%% ignorenonframetext does not work
% \mode<article>{}
 % \mode<presentation>{\usetheme{default} }
 
 \input{headerFiles/header}
 \input{headerFiles/math-def}
 \newtheorem{stylfact}{Stylized fact}
 \input{headerFiles/tikz-def}
 \input{headerFiles/acronyms}

\renewcommand{\Xi}{\cola{\omega}}
 
\setbeamerfont{myTOC}{series=\bfseries,size=\Large}
\AtBeginSection[]{\frame{\frametitle{Outline}%
                  \usebeamerfont{myTOC}\tableofcontents[current]}}

 % opening
 % \only<article>{\title{The role of the interbank market in a macroeconomic model with financial frictions}}
\title{Financial frictions and the interbank market}
 
 \author{Fabian Greimel}
 \institute{%Institut f\"ur h\"ohere Studien---
   Master's thesis}


\begin{document}

\begin{tikzpicture}[x=1pt,y=1pt]
\definecolor{fillColor}{RGB}{255,255,255}
\path[use as bounding box,fill=fillColor,fill opacity=0.00] (0,0) rectangle (505.89,216.81);
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{RGB}{255,255,255}
\definecolor{fillColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round,line cap=round,fill=fillColor] (  0.00,  0.00) rectangle (505.89,216.81);
\end{scope}
\begin{scope}
\path[clip] ( 58.98,121.93) rectangle (178.18,190.33);
\definecolor{fillColor}{gray}{0.90}

\path[fill=fillColor] ( 58.98,121.93) rectangle (178.18,190.33);
\definecolor{drawColor}{gray}{0.95}

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98,134.11) --
	(178.18,134.11);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98,147.95) --
	(178.18,147.95);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98,161.79) --
	(178.18,161.79);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98,175.63) --
	(178.18,175.63);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98,189.47) --
	(178.18,189.47);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 77.97,121.93) --
	( 77.97,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (105.11,121.93) --
	(105.11,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (132.26,121.93) --
	(132.26,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (159.40,121.93) --
	(159.40,190.33);
\definecolor{drawColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98,127.19) --
	(178.18,127.19);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98,141.03) --
	(178.18,141.03);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98,154.87) --
	(178.18,154.87);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98,168.71) --
	(178.18,168.71);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98,182.55) --
	(178.18,182.55);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 64.40,121.93) --
	( 64.40,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 91.54,121.93) --
	( 91.54,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (118.69,121.93) --
	(118.69,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (145.83,121.93) --
	(145.83,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (172.98,121.93) --
	(172.98,190.33);
\definecolor{fillColor}{RGB}{0,0,0}

\path[fill=fillColor] ( 64.40,125.04) circle (  2.13);

\path[fill=fillColor] ( 75.25,126.03) circle (  2.13);

\path[fill=fillColor] ( 86.11,128.08) circle (  2.13);

\path[fill=fillColor] ( 96.97,131.24) circle (  2.13);

\path[fill=fillColor] (107.83,135.52) circle (  2.13);

\path[fill=fillColor] (118.69,140.97) circle (  2.13);

\path[fill=fillColor] (129.54,147.62) circle (  2.13);

\path[fill=fillColor] (140.40,155.53) circle (  2.13);

\path[fill=fillColor] (151.26,164.77) circle (  2.13);

\path[fill=fillColor] (162.12,175.39) circle (  2.13);

\path[fill=fillColor] (172.76,187.22) circle (  2.13);
\end{scope}
\begin{scope}
\path[clip] (214.02,121.93) rectangle (333.22,190.33);
\definecolor{fillColor}{gray}{0.90}

\path[fill=fillColor] (214.02,121.93) rectangle (333.22,190.33);
\definecolor{drawColor}{gray}{0.95}

\path[draw=drawColor,line width= 0.3pt,line join=round] (214.02,132.93) --
	(333.22,132.93);

\path[draw=drawColor,line width= 0.3pt,line join=round] (214.02,152.35) --
	(333.22,152.35);

\path[draw=drawColor,line width= 0.3pt,line join=round] (214.02,171.77) --
	(333.22,171.77);

\path[draw=drawColor,line width= 0.3pt,line join=round] (233.01,121.93) --
	(233.01,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (260.16,121.93) --
	(260.16,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (287.30,121.93) --
	(287.30,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (314.45,121.93) --
	(314.45,190.33);
\definecolor{drawColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round] (214.02,123.22) --
	(333.22,123.22);

\path[draw=drawColor,line width= 0.6pt,line join=round] (214.02,142.64) --
	(333.22,142.64);

\path[draw=drawColor,line width= 0.6pt,line join=round] (214.02,162.06) --
	(333.22,162.06);

\path[draw=drawColor,line width= 0.6pt,line join=round] (214.02,181.48) --
	(333.22,181.48);

\path[draw=drawColor,line width= 0.6pt,line join=round] (219.44,121.93) --
	(219.44,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (246.58,121.93) --
	(246.58,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (273.73,121.93) --
	(273.73,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (300.87,121.93) --
	(300.87,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (328.02,121.93) --
	(328.02,190.33);
\definecolor{fillColor}{RGB}{0,0,0}

\path[fill=fillColor] (219.44,125.04) circle (  2.13);

\path[fill=fillColor] (230.30,126.03) circle (  2.13);

\path[fill=fillColor] (241.15,128.10) circle (  2.13);

\path[fill=fillColor] (252.01,131.27) circle (  2.13);

\path[fill=fillColor] (262.87,135.56) circle (  2.13);

\path[fill=fillColor] (273.73,141.02) circle (  2.13);

\path[fill=fillColor] (284.59,147.69) circle (  2.13);

\path[fill=fillColor] (295.45,155.60) circle (  2.13);

\path[fill=fillColor] (306.30,164.83) circle (  2.13);

\path[fill=fillColor] (317.16,175.43) circle (  2.13);

\path[fill=fillColor] (327.80,187.22) circle (  2.13);
\end{scope}
\begin{scope}
\path[clip] (374.64,121.93) rectangle (493.85,190.33);
\definecolor{fillColor}{gray}{0.90}

\path[fill=fillColor] (374.64,121.93) rectangle (493.85,190.33);
\definecolor{drawColor}{gray}{0.95}

\path[draw=drawColor,line width= 0.3pt,line join=round] (374.64,129.60) --
	(493.85,129.60);

\path[draw=drawColor,line width= 0.3pt,line join=round] (374.64,141.63) --
	(493.85,141.63);

\path[draw=drawColor,line width= 0.3pt,line join=round] (374.64,153.66) --
	(493.85,153.66);

\path[draw=drawColor,line width= 0.3pt,line join=round] (374.64,165.70) --
	(493.85,165.70);

\path[draw=drawColor,line width= 0.3pt,line join=round] (374.64,177.73) --
	(493.85,177.73);

\path[draw=drawColor,line width= 0.3pt,line join=round] (374.64,189.77) --
	(493.85,189.77);

\path[draw=drawColor,line width= 0.3pt,line join=round] (393.63,121.93) --
	(393.63,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (420.78,121.93) --
	(420.78,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (447.93,121.93) --
	(447.93,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (475.07,121.93) --
	(475.07,190.33);
\definecolor{drawColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64,123.58) --
	(493.85,123.58);

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64,135.61) --
	(493.85,135.61);

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64,147.65) --
	(493.85,147.65);

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64,159.68) --
	(493.85,159.68);

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64,171.72) --
	(493.85,171.72);

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64,183.75) --
	(493.85,183.75);

\path[draw=drawColor,line width= 0.6pt,line join=round] (380.06,121.93) --
	(380.06,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (407.21,121.93) --
	(407.21,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (434.35,121.93) --
	(434.35,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (461.50,121.93) --
	(461.50,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (488.64,121.93) --
	(488.64,190.33);
\definecolor{fillColor}{RGB}{0,0,0}

\path[fill=fillColor] (380.06,125.04) circle (  2.13);

\path[fill=fillColor] (390.92,126.05) circle (  2.13);

\path[fill=fillColor] (401.78,128.14) circle (  2.13);

\path[fill=fillColor] (412.64,131.35) circle (  2.13);

\path[fill=fillColor] (423.49,135.69) circle (  2.13);

\path[fill=fillColor] (434.35,141.19) circle (  2.13);

\path[fill=fillColor] (445.21,147.89) circle (  2.13);

\path[fill=fillColor] (456.07,155.83) circle (  2.13);

\path[fill=fillColor] (466.93,165.03) circle (  2.13);

\path[fill=fillColor] (477.79,175.57) circle (  2.13);

\path[fill=fillColor] (488.43,187.22) circle (  2.13);
\end{scope}
\begin{scope}
\path[clip] ( 58.98, 35.49) rectangle (178.18,103.89);
\definecolor{fillColor}{gray}{0.90}

\path[fill=fillColor] ( 58.98, 35.49) rectangle (178.18,103.89);
\definecolor{drawColor}{gray}{0.95}

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98, 38.74) --
	(178.18, 38.74);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98, 54.11) --
	(178.18, 54.11);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98, 69.48) --
	(178.18, 69.48);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98, 84.85) --
	(178.18, 84.85);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98,100.22) --
	(178.18,100.22);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 77.97, 35.49) --
	( 77.97,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (105.11, 35.49) --
	(105.11,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (132.26, 35.49) --
	(132.26,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (159.40, 35.49) --
	(159.40,103.89);
\definecolor{drawColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98, 46.43) --
	(178.18, 46.43);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98, 61.79) --
	(178.18, 61.79);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98, 77.16) --
	(178.18, 77.16);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98, 92.53) --
	(178.18, 92.53);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 64.40, 35.49) --
	( 64.40,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 91.54, 35.49) --
	( 91.54,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (118.69, 35.49) --
	(118.69,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (145.83, 35.49) --
	(145.83,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (172.98, 35.49) --
	(172.98,103.89);
\definecolor{fillColor}{RGB}{0,0,0}

\path[fill=fillColor] ( 64.40, 85.57) circle (  2.13);

\path[fill=fillColor] ( 75.25, 93.26) circle (  2.13);

\path[fill=fillColor] ( 86.11, 98.39) circle (  2.13);

\path[fill=fillColor] ( 96.97,100.78) circle (  2.13);

\path[fill=fillColor] (107.83,100.29) circle (  2.13);

\path[fill=fillColor] (118.69, 96.85) circle (  2.13);

\path[fill=fillColor] (129.54, 90.48) circle (  2.13);

\path[fill=fillColor] (140.40, 81.25) circle (  2.13);

\path[fill=fillColor] (151.26, 69.32) circle (  2.13);

\path[fill=fillColor] (162.12, 54.89) circle (  2.13);

\path[fill=fillColor] (172.76, 38.60) circle (  2.13);
\end{scope}
\begin{scope}
\path[clip] (214.02, 35.49) rectangle (333.22,103.89);
\definecolor{fillColor}{gray}{0.90}

\path[fill=fillColor] (214.02, 35.49) rectangle (333.22,103.89);
\definecolor{drawColor}{gray}{0.95}

\path[draw=drawColor,line width= 0.3pt,line join=round] (214.02, 42.58) --
	(333.22, 42.58);

\path[draw=drawColor,line width= 0.3pt,line join=round] (214.02, 64.63) --
	(333.22, 64.63);

\path[draw=drawColor,line width= 0.3pt,line join=round] (214.02, 86.69) --
	(333.22, 86.69);

\path[draw=drawColor,line width= 0.3pt,line join=round] (233.01, 35.49) --
	(233.01,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (260.16, 35.49) --
	(260.16,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (287.30, 35.49) --
	(287.30,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (314.45, 35.49) --
	(314.45,103.89);
\definecolor{drawColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round] (214.02, 53.60) --
	(333.22, 53.60);

\path[draw=drawColor,line width= 0.6pt,line join=round] (214.02, 75.66) --
	(333.22, 75.66);

\path[draw=drawColor,line width= 0.6pt,line join=round] (214.02, 97.72) --
	(333.22, 97.72);

\path[draw=drawColor,line width= 0.6pt,line join=round] (219.44, 35.49) --
	(219.44,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (246.58, 35.49) --
	(246.58,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (273.73, 35.49) --
	(273.73,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (300.87, 35.49) --
	(300.87,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (328.02, 35.49) --
	(328.02,103.89);
\definecolor{fillColor}{RGB}{0,0,0}

\path[fill=fillColor] (219.44,100.78) circle (  2.13);

\path[fill=fillColor] (230.30, 79.43) circle (  2.13);

\path[fill=fillColor] (241.15, 62.49) circle (  2.13);

\path[fill=fillColor] (252.01, 50.01) circle (  2.13);

\path[fill=fillColor] (262.87, 42.01) circle (  2.13);

\path[fill=fillColor] (273.73, 38.60) circle (  2.13);

\path[fill=fillColor] (284.59, 39.88) circle (  2.13);

\path[fill=fillColor] (295.45, 45.98) circle (  2.13);

\path[fill=fillColor] (306.30, 57.10) circle (  2.13);

\path[fill=fillColor] (317.16, 73.43) circle (  2.13);

\path[fill=fillColor] (327.80, 94.74) circle (  2.13);
\end{scope}
\begin{scope}
\path[clip] (374.64, 35.49) rectangle (493.85,103.89);
\definecolor{fillColor}{gray}{0.90}

\path[fill=fillColor] (374.64, 35.49) rectangle (493.85,103.89);
\definecolor{drawColor}{gray}{0.95}

\path[draw=drawColor,line width= 0.3pt,line join=round] (374.64, 44.82) --
	(493.85, 44.82);

\path[draw=drawColor,line width= 0.3pt,line join=round] (374.64, 57.27) --
	(493.85, 57.27);

\path[draw=drawColor,line width= 0.3pt,line join=round] (374.64, 69.71) --
	(493.85, 69.71);

\path[draw=drawColor,line width= 0.3pt,line join=round] (374.64, 82.16) --
	(493.85, 82.16);

\path[draw=drawColor,line width= 0.3pt,line join=round] (374.64, 94.61) --
	(493.85, 94.61);

\path[draw=drawColor,line width= 0.3pt,line join=round] (393.63, 35.49) --
	(393.63,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (420.78, 35.49) --
	(420.78,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (447.93, 35.49) --
	(447.93,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (475.07, 35.49) --
	(475.07,103.89);
\definecolor{drawColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64, 38.60) --
	(493.85, 38.60);

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64, 51.04) --
	(493.85, 51.04);

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64, 63.49) --
	(493.85, 63.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64, 75.94) --
	(493.85, 75.94);

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64, 88.38) --
	(493.85, 88.38);

\path[draw=drawColor,line width= 0.6pt,line join=round] (374.64,100.83) --
	(493.85,100.83);

\path[draw=drawColor,line width= 0.6pt,line join=round] (380.06, 35.49) --
	(380.06,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (407.21, 35.49) --
	(407.21,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (434.35, 35.49) --
	(434.35,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (461.50, 35.49) --
	(461.50,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (488.64, 35.49) --
	(488.64,103.89);
\definecolor{fillColor}{RGB}{0,0,0}

\path[fill=fillColor] (380.06, 38.60) circle (  2.13);

\path[fill=fillColor] (390.92, 45.86) circle (  2.13);

\path[fill=fillColor] (401.78, 53.28) circle (  2.13);

\path[fill=fillColor] (412.64, 60.73) circle (  2.13);

\path[fill=fillColor] (423.49, 68.05) circle (  2.13);

\path[fill=fillColor] (434.35, 75.11) circle (  2.13);

\path[fill=fillColor] (445.21, 81.76) circle (  2.13);

\path[fill=fillColor] (456.07, 87.82) circle (  2.13);

\path[fill=fillColor] (466.93, 93.15) circle (  2.13);

\path[fill=fillColor] (477.79, 97.55) circle (  2.13);

\path[fill=fillColor] (488.43,100.78) circle (  2.13);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{fillColor}{gray}{0.80}

\path[fill=fillColor] ( 58.98,190.33) rectangle (178.18,204.76);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (118.58,193.90) {labor $L$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{fillColor}{gray}{0.80}

\path[fill=fillColor] (214.02,190.33) rectangle (333.22,204.76);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (273.62,193.90) {output $Y$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{fillColor}{gray}{0.80}

\path[fill=fillColor] (374.64,190.33) rectangle (493.85,204.76);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (434.24,193.90) {consumption $C$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{fillColor}{gray}{0.80}

\path[fill=fillColor] ( 58.98,103.89) rectangle (178.18,118.32);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (118.58,107.45) {friction $\cola{\theta}$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{fillColor}{gray}{0.80}

\path[fill=fillColor] (214.02,103.89) rectangle (333.22,118.32);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (273.62,107.45) {deposits $D$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{fillColor}{gray}{0.80}

\path[fill=fillColor] (374.64,103.89) rectangle (493.85,118.32);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (434.24,107.45) {interbank spread $R_b - R$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86,123.54) {0.2385};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86,137.38) {0.2390};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86,151.22) {0.2395};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86,165.06) {0.2400};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86,178.90) {0.2405};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71,127.19) --
	( 58.98,127.19);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71,141.03) --
	( 58.98,141.03);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71,154.87) --
	( 58.98,154.87);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71,168.71) --
	( 58.98,168.71);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71,182.55) --
	( 58.98,182.55);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (206.91,119.57) {0.685};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (206.91,138.99) {0.690};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (206.91,158.41) {0.695};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (206.91,177.83) {0.700};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] (209.75,123.22) --
	(214.02,123.22);

\path[draw=drawColor,line width= 0.6pt,line join=round] (209.75,142.64) --
	(214.02,142.64);

\path[draw=drawColor,line width= 0.6pt,line join=round] (209.75,162.06) --
	(214.02,162.06);

\path[draw=drawColor,line width= 0.6pt,line join=round] (209.75,181.48) --
	(214.02,181.48);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53,119.93) {0.402};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53,131.96) {0.403};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53,144.00) {0.404};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53,156.03) {0.405};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53,168.07) {0.406};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53,180.10) {0.407};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37,123.58) --
	(374.64,123.58);

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37,135.61) --
	(374.64,135.61);

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37,147.65) --
	(374.64,147.65);

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37,159.68) --
	(374.64,159.68);

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37,171.72) --
	(374.64,171.72);

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37,183.75) --
	(374.64,183.75);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86, 42.78) {0.39};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86, 58.14) {0.40};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86, 73.51) {0.41};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86, 88.88) {0.42};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71, 46.43) --
	( 58.98, 46.43);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71, 61.79) --
	( 58.98, 61.79);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71, 77.16) --
	( 58.98, 77.16);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71, 92.53) --
	( 58.98, 92.53);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (206.91, 49.95) {4.58};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (206.91, 72.01) {4.60};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (206.91, 94.07) {4.62};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] (209.75, 53.60) --
	(214.02, 53.60);

\path[draw=drawColor,line width= 0.6pt,line join=round] (209.75, 75.66) --
	(214.02, 75.66);

\path[draw=drawColor,line width= 0.6pt,line join=round] (209.75, 97.72) --
	(214.02, 97.72);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53, 34.95) {0.0000};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53, 47.39) {0.0005};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53, 59.84) {0.0010};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53, 72.29) {0.0015};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53, 84.73) {0.0020};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (367.53, 97.18) {0.0025};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37, 38.60) --
	(374.64, 38.60);

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37, 51.04) --
	(374.64, 51.04);

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37, 63.49) --
	(374.64, 63.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37, 75.94) --
	(374.64, 75.94);

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37, 88.38) --
	(374.64, 88.38);

\path[draw=drawColor,line width= 0.6pt,line join=round] (370.37,100.83) --
	(374.64,100.83);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 64.40, 31.22) --
	( 64.40, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 91.54, 31.22) --
	( 91.54, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (118.69, 31.22) --
	(118.69, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (145.83, 31.22) --
	(145.83, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (172.98, 31.22) --
	(172.98, 35.49);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 64.40, 21.08) {0.00};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 91.54, 21.08) {0.25};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (118.69, 21.08) {0.50};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (145.83, 21.08) {0.75};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (172.98, 21.08) {1.00};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] (219.44, 31.22) --
	(219.44, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (246.58, 31.22) --
	(246.58, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (273.73, 31.22) --
	(273.73, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (300.87, 31.22) --
	(300.87, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (328.02, 31.22) --
	(328.02, 35.49);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (219.44, 21.08) {0.00};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (246.58, 21.08) {0.25};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (273.73, 21.08) {0.50};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (300.87, 21.08) {0.75};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (328.02, 21.08) {1.00};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] (380.06, 31.22) --
	(380.06, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (407.21, 31.22) --
	(407.21, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (434.35, 31.22) --
	(434.35, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (461.50, 31.22) --
	(461.50, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (488.64, 31.22) --
	(488.64, 35.49);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (380.06, 21.08) {0.00};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (407.21, 21.08) {0.25};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (434.35, 21.08) {0.50};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (461.50, 21.08) {0.75};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (488.64, 21.08) {1.00};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.20] at (276.41,  9.03) {the steady state degree of interbank friction $\cola{\omega}$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,216.81);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,rotate= 90.00,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.20] at ( 18.16,112.91) {steady state values};
\end{scope}
\end{tikzpicture}

\end{document}
