% Created by tikzDevice version 0.8.1 on 2015-06-29 10:20:23
% !TEX encoding = UTF-8 Unicode
\documentclass[mathserif,x11names,dvipsnames,red]{beamer}
\nonstopmode

\usepackage{tikz}

\usepackage[active,tightpage,psfixbb]{preview}

\PreviewEnvironment{pgfpicture}

\setlength\PreviewBorder{0pt}

\newcommand{\SweaveOpts}[1]{}  % do not interfere with LaTeX
\newcommand{\SweaveInput}[1]{} % because they are not real TeX commands
\newcommand{\Sexpr}[1]{}       % will only be parsed by R


\def\w{$\omega$}

%\usefonttheme{serif}
\usepackage{etex}
\usepackage{appendixnumberbeamer}
\newcommand{\backupbegin}{
   \newcounter{framenumberappendix}
   \setcounter{framenumberappendix}{\value{framenumber}}
}
\newcommand{\backupend}{  \addtocounter{framenumberappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumberappendix}} 
}

\setbeameroption{hide notes} % Only slides
%\setbeameroption{show notes} % show notes 
%\setbeameroption{show only notes} % Only notes

\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

%% ignorenonframetext does not work
% \mode<article>{}
 % \mode<presentation>{\usetheme{default} }
 
 \input{headerFiles/header}
 \input{headerFiles/math-def}
 \newtheorem{stylfact}{Stylized fact}
 \input{headerFiles/tikz-def}
 \input{headerFiles/acronyms}

\renewcommand{\Xi}{\cola{\omega}}
 
\setbeamerfont{myTOC}{series=\bfseries,size=\Large}
\AtBeginSection[]{\frame{\frametitle{Outline}%
                  \usebeamerfont{myTOC}\tableofcontents[current]}}

 % opening
 % \only<article>{\title{The role of the interbank market in a macroeconomic model with financial frictions}}
\title{Financial frictions and the interbank market}
 
 \author{Fabian Greimel}
 \institute{%Institut f\"ur h\"ohere Studien---
   Master's thesis}


\begin{document}

\begin{tikzpicture}[x=1pt,y=1pt]
\definecolor{fillColor}{RGB}{255,255,255}
\path[use as bounding box,fill=fillColor,fill opacity=0.00] (0,0) rectangle (361.35,216.81);
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{RGB}{255,255,255}
\definecolor{fillColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round,line cap=round,fill=fillColor] (  0.00,  0.00) rectangle (361.35,216.81);
\end{scope}
\begin{scope}
\path[clip] ( 58.98,121.93) rectangle (183.29,190.33);
\definecolor{fillColor}{gray}{0.90}

\path[fill=fillColor] ( 58.98,121.93) rectangle (183.29,190.33);
\definecolor{drawColor}{gray}{0.95}

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98,133.42) --
	(183.29,133.42);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98,150.42) --
	(183.29,150.42);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98,167.42) --
	(183.29,167.42);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98,184.43) --
	(183.29,184.43);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 78.78,121.93) --
	( 78.78,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (107.09,121.93) --
	(107.09,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (135.40,121.93) --
	(135.40,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (163.71,121.93) --
	(163.71,190.33);
\definecolor{drawColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98,124.92) --
	(183.29,124.92);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98,141.92) --
	(183.29,141.92);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98,158.92) --
	(183.29,158.92);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98,175.92) --
	(183.29,175.92);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 64.63,121.93) --
	( 64.63,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 92.94,121.93) --
	( 92.94,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (121.25,121.93) --
	(121.25,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (149.56,121.93) --
	(149.56,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (177.87,121.93) --
	(177.87,190.33);
\definecolor{fillColor}{RGB}{0,0,0}

\path[fill=fillColor] ( 64.63,187.22) circle (  2.13);

\path[fill=fillColor] ( 75.95,181.09) circle (  2.13);

\path[fill=fillColor] ( 87.27,174.94) circle (  2.13);

\path[fill=fillColor] ( 98.60,168.77) circle (  2.13);

\path[fill=fillColor] (109.92,162.58) circle (  2.13);

\path[fill=fillColor] (121.25,156.37) circle (  2.13);

\path[fill=fillColor] (132.57,150.14) circle (  2.13);

\path[fill=fillColor] (143.89,143.87) circle (  2.13);

\path[fill=fillColor] (155.22,137.59) circle (  2.13);

\path[fill=fillColor] (166.54,131.27) circle (  2.13);

\path[fill=fillColor] (177.64,125.04) circle (  2.13);
\end{scope}
\begin{scope}
\path[clip] (224.99,121.93) rectangle (349.30,190.33);
\definecolor{fillColor}{gray}{0.90}

\path[fill=fillColor] (224.99,121.93) rectangle (349.30,190.33);
\definecolor{drawColor}{gray}{0.95}

\path[draw=drawColor,line width= 0.3pt,line join=round] (224.99,124.22) --
	(349.30,124.22);

\path[draw=drawColor,line width= 0.3pt,line join=round] (224.99,138.14) --
	(349.30,138.14);

\path[draw=drawColor,line width= 0.3pt,line join=round] (224.99,152.05) --
	(349.30,152.05);

\path[draw=drawColor,line width= 0.3pt,line join=round] (224.99,165.97) --
	(349.30,165.97);

\path[draw=drawColor,line width= 0.3pt,line join=round] (224.99,179.88) --
	(349.30,179.88);

\path[draw=drawColor,line width= 0.3pt,line join=round] (244.80,121.93) --
	(244.80,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (273.11,121.93) --
	(273.11,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (301.42,121.93) --
	(301.42,190.33);

\path[draw=drawColor,line width= 0.3pt,line join=round] (329.73,121.93) --
	(329.73,190.33);
\definecolor{drawColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round] (224.99,131.18) --
	(349.30,131.18);

\path[draw=drawColor,line width= 0.6pt,line join=round] (224.99,145.10) --
	(349.30,145.10);

\path[draw=drawColor,line width= 0.6pt,line join=round] (224.99,159.01) --
	(349.30,159.01);

\path[draw=drawColor,line width= 0.6pt,line join=round] (224.99,172.93) --
	(349.30,172.93);

\path[draw=drawColor,line width= 0.6pt,line join=round] (224.99,186.84) --
	(349.30,186.84);

\path[draw=drawColor,line width= 0.6pt,line join=round] (230.64,121.93) --
	(230.64,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (258.95,121.93) --
	(258.95,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (287.26,121.93) --
	(287.26,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (315.57,121.93) --
	(315.57,190.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] (343.88,121.93) --
	(343.88,190.33);
\definecolor{fillColor}{RGB}{0,0,0}

\path[fill=fillColor] (230.64,125.04) circle (  2.13);

\path[fill=fillColor] (241.97,126.02) circle (  2.13);

\path[fill=fillColor] (253.29,128.07) circle (  2.13);

\path[fill=fillColor] (264.61,131.21) circle (  2.13);

\path[fill=fillColor] (275.94,135.48) circle (  2.13);

\path[fill=fillColor] (287.26,140.91) circle (  2.13);

\path[fill=fillColor] (298.59,147.55) circle (  2.13);

\path[fill=fillColor] (309.91,155.45) circle (  2.13);

\path[fill=fillColor] (321.23,164.69) circle (  2.13);

\path[fill=fillColor] (332.56,175.34) circle (  2.13);

\path[fill=fillColor] (343.65,187.22) circle (  2.13);
\end{scope}
\begin{scope}
\path[clip] ( 58.98, 35.49) rectangle (183.29,103.89);
\definecolor{fillColor}{gray}{0.90}

\path[fill=fillColor] ( 58.98, 35.49) rectangle (183.29,103.89);
\definecolor{drawColor}{gray}{0.95}

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98, 49.37) --
	(183.29, 49.37);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98, 65.29) --
	(183.29, 65.29);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98, 81.21) --
	(183.29, 81.21);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 58.98, 97.13) --
	(183.29, 97.13);

\path[draw=drawColor,line width= 0.3pt,line join=round] ( 78.78, 35.49) --
	( 78.78,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (107.09, 35.49) --
	(107.09,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (135.40, 35.49) --
	(135.40,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (163.71, 35.49) --
	(163.71,103.89);
\definecolor{drawColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98, 41.41) --
	(183.29, 41.41);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98, 57.33) --
	(183.29, 57.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98, 73.25) --
	(183.29, 73.25);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 58.98, 89.17) --
	(183.29, 89.17);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 64.63, 35.49) --
	( 64.63,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 92.94, 35.49) --
	( 92.94,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (121.25, 35.49) --
	(121.25,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (149.56, 35.49) --
	(149.56,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (177.87, 35.49) --
	(177.87,103.89);
\definecolor{fillColor}{RGB}{0,0,0}

\path[fill=fillColor] ( 64.63, 91.19) circle (  2.13);

\path[fill=fillColor] ( 75.95, 96.58) circle (  2.13);

\path[fill=fillColor] ( 87.27, 99.80) circle (  2.13);

\path[fill=fillColor] ( 98.60,100.78) circle (  2.13);

\path[fill=fillColor] (109.92, 99.44) circle (  2.13);

\path[fill=fillColor] (121.25, 95.72) circle (  2.13);

\path[fill=fillColor] (132.57, 89.53) circle (  2.13);

\path[fill=fillColor] (143.89, 80.78) circle (  2.13);

\path[fill=fillColor] (155.22, 69.38) circle (  2.13);

\path[fill=fillColor] (166.54, 55.23) circle (  2.13);

\path[fill=fillColor] (177.64, 38.60) circle (  2.13);
\end{scope}
\begin{scope}
\path[clip] (224.99, 35.49) rectangle (349.30,103.89);
\definecolor{fillColor}{gray}{0.90}

\path[fill=fillColor] (224.99, 35.49) rectangle (349.30,103.89);
\definecolor{drawColor}{gray}{0.95}

\path[draw=drawColor,line width= 0.3pt,line join=round] (224.99, 41.92) --
	(349.30, 41.92);

\path[draw=drawColor,line width= 0.3pt,line join=round] (224.99, 57.89) --
	(349.30, 57.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (224.99, 73.87) --
	(349.30, 73.87);

\path[draw=drawColor,line width= 0.3pt,line join=round] (224.99, 89.85) --
	(349.30, 89.85);

\path[draw=drawColor,line width= 0.3pt,line join=round] (244.80, 35.49) --
	(244.80,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (273.11, 35.49) --
	(273.11,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (301.42, 35.49) --
	(301.42,103.89);

\path[draw=drawColor,line width= 0.3pt,line join=round] (329.73, 35.49) --
	(329.73,103.89);
\definecolor{drawColor}{RGB}{255,255,255}

\path[draw=drawColor,line width= 0.6pt,line join=round] (224.99, 49.91) --
	(349.30, 49.91);

\path[draw=drawColor,line width= 0.6pt,line join=round] (224.99, 65.88) --
	(349.30, 65.88);

\path[draw=drawColor,line width= 0.6pt,line join=round] (224.99, 81.86) --
	(349.30, 81.86);

\path[draw=drawColor,line width= 0.6pt,line join=round] (224.99, 97.84) --
	(349.30, 97.84);

\path[draw=drawColor,line width= 0.6pt,line join=round] (230.64, 35.49) --
	(230.64,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (258.95, 35.49) --
	(258.95,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (287.26, 35.49) --
	(287.26,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (315.57, 35.49) --
	(315.57,103.89);

\path[draw=drawColor,line width= 0.6pt,line join=round] (343.88, 35.49) --
	(343.88,103.89);
\definecolor{fillColor}{RGB}{0,0,0}

\path[fill=fillColor] (230.64,100.78) circle (  2.13);

\path[fill=fillColor] (241.97, 98.48) circle (  2.13);

\path[fill=fillColor] (253.29, 95.38) circle (  2.13);

\path[fill=fillColor] (264.61, 91.47) circle (  2.13);

\path[fill=fillColor] (275.94, 86.71) circle (  2.13);

\path[fill=fillColor] (287.26, 81.09) circle (  2.13);

\path[fill=fillColor] (298.59, 74.55) circle (  2.13);

\path[fill=fillColor] (309.91, 67.07) circle (  2.13);

\path[fill=fillColor] (321.23, 58.59) circle (  2.13);

\path[fill=fillColor] (332.56, 49.04) circle (  2.13);

\path[fill=fillColor] (343.65, 38.60) circle (  2.13);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{fillColor}{gray}{0.80}

\path[fill=fillColor] ( 58.98,190.33) rectangle (183.29,204.76);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (121.13,193.90) {asset price $Q^n_i$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{fillColor}{gray}{0.80}

\path[fill=fillColor] (224.99,190.33) rectangle (349.30,204.76);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (287.15,193.90) {loans $S^n$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{fillColor}{gray}{0.80}

\path[fill=fillColor] ( 58.98,103.89) rectangle (183.29,118.32);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (121.13,107.45) {multiplier $\bar \lambda$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{fillColor}{gray}{0.80}

\path[fill=fillColor] (224.99,103.89) rectangle (349.30,118.32);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (287.15,107.45) {interbank trade $B^n$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86,121.27) {1.00};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86,138.27) {1.02};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86,155.27) {1.04};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86,172.28) {1.06};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71,124.92) --
	( 58.98,124.92);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71,141.92) --
	( 58.98,141.92);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71,158.92) --
	( 58.98,158.92);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71,175.92) --
	( 58.98,175.92);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (217.88,127.53) {4.30};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (217.88,141.45) {4.35};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (217.88,155.36) {4.40};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (217.88,169.28) {4.45};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (217.88,183.19) {4.50};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] (220.72,131.18) --
	(224.99,131.18);

\path[draw=drawColor,line width= 0.6pt,line join=round] (220.72,145.10) --
	(224.99,145.10);

\path[draw=drawColor,line width= 0.6pt,line join=round] (220.72,159.01) --
	(224.99,159.01);

\path[draw=drawColor,line width= 0.6pt,line join=round] (220.72,172.93) --
	(224.99,172.93);

\path[draw=drawColor,line width= 0.6pt,line join=round] (220.72,186.84) --
	(224.99,186.84);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86, 37.76) {0.0100};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86, 53.68) {0.0105};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86, 69.60) {0.0110};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 51.86, 85.52) {0.0115};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71, 41.41) --
	( 58.98, 41.41);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71, 57.33) --
	( 58.98, 57.33);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71, 73.25) --
	( 58.98, 73.25);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 54.71, 89.17) --
	( 58.98, 89.17);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (217.88, 46.26) {-0.114};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (217.88, 62.23) {-0.112};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (217.88, 78.21) {-0.110};

\node[text=drawColor,anchor=base east,inner sep=0pt, outer sep=0pt, scale=  0.96] at (217.88, 94.19) {-0.108};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] (220.72, 49.91) --
	(224.99, 49.91);

\path[draw=drawColor,line width= 0.6pt,line join=round] (220.72, 65.88) --
	(224.99, 65.88);

\path[draw=drawColor,line width= 0.6pt,line join=round] (220.72, 81.86) --
	(224.99, 81.86);

\path[draw=drawColor,line width= 0.6pt,line join=round] (220.72, 97.84) --
	(224.99, 97.84);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 64.63, 31.22) --
	( 64.63, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] ( 92.94, 31.22) --
	( 92.94, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (121.25, 31.22) --
	(121.25, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (149.56, 31.22) --
	(149.56, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (177.87, 31.22) --
	(177.87, 35.49);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 64.63, 21.08) {0.00};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at ( 92.94, 21.08) {0.25};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (121.25, 21.08) {0.50};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (149.56, 21.08) {0.75};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (177.87, 21.08) {1.00};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\path[draw=drawColor,line width= 0.6pt,line join=round] (230.64, 31.22) --
	(230.64, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (258.95, 31.22) --
	(258.95, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (287.26, 31.22) --
	(287.26, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (315.57, 31.22) --
	(315.57, 35.49);

\path[draw=drawColor,line width= 0.6pt,line join=round] (343.88, 31.22) --
	(343.88, 35.49);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{gray}{0.50}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (230.64, 21.08) {0.00};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (258.95, 21.08) {0.25};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (287.26, 21.08) {0.50};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (315.57, 21.08) {0.75};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  0.96] at (343.88, 21.08) {1.00};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.20] at (204.14,  9.03) {the steady state degree of interbank friction $\cola{\omega}$};
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (361.35,216.81);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,rotate= 90.00,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.20] at ( 18.16,112.91) {steady state values};
\end{scope}
\end{tikzpicture}

\end{document}
