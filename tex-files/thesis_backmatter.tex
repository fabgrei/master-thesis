

\section{Conclusions and outlook}
\label{sec:conclusion}

I have shown that the interbank market plays only a minor role in the given model framework. This has implications for policy makers and points to promising research topics.

\subsection{Implication for monetary policy}

An important contribution of \citet{gertler2011handbook} is to provide a framework to analyze unconventional monetary policy. The setup is similar to \citet{gertler2011unconventional,gertler2013qe}---who have an even stronger focus on monetary policy---but it provides additional insight due to the existence of an interbank market.

In general, unconventional monetary policies (as opposed to interest rate policy) are measures that are taken to extend the liquidity in the economy. A famous instance are the \ac{qe} programs by the \ac{fed}. The given framework provides a new way to think about the mechanism of these policies: Unconventional monetary policy aims at relaxing the lending constraints of the banks. There are different ways how the central bank can intervene. From a conceptual point of view, the simplest measures are lending facilities (direct lending), where the central bank lends to firms directly. This mechanism is not only used by \citet{gertler2011handbook}, but also by \citet{gertler2011unconventional,gertler2013qe}, and the findings of this Master thesis do not affect its relevance.

\citet{gertler2011handbook} additionally discuss two more credit policies: $(i)$ discount facilities, where the banks can borrow from the central bank, with a lower degree of friction. This policy strengthens the balance sheet of the banks through external fundings. And $(ii)$ equity injections, where the central bank acquires bank net worth. Unfortunately, \citet{gertler2011handbook} do not provide a quantitative analysis of the policies. I conjecture that the discount facilities will hardly have an effect, because they are modelled in the same way as interbank lending. Discount facilities can be thought of as an alternative to interbank trade.

The role of the interbank market is certainly more important than this model suggests. So it is certainly important how monetary policy can intervene, if the interbank market dries out. It is, thus, essential to change the model structure in way that gives more weight to the interbank market. 

\subsection{Giving the interbank market a larger role}

Anecdotal evidence suggest that the interbank market has played an important role in the propagation of the financial crisis 2007--2009. In the given model the role of the interbank market is very limited since it is too small. In this section I point at possible paths to go.

\paragraph{Is the quarterly frequency adequate?} As discussed earlier, the daily flows on the interbank market are very large compared to stocks. For 1998 it was estimated that the aggregate flows of two days exceed the stocks. This suggests that a quarterly frequency is inadequate, since it cannot mirror the comparatively large flows. However---without any other changes---increasing the frequency of the model will not help. I have shown that interbank lending is (under reasonable parameters) bounded by investment, and investment is $\delta K^*$ in steady state. When going to monthly or daily frequency, the depreciation rate will fall accordingly. This puts an ever lower bound on interbank lending. Thus, the tight link of $\delta$ and interbank lending has to be broken. This is discussed below.

\paragraph{Increasing relative weight} In the current model the share of interbank lending to total credit is very small. That is because banks have to roll over loans worth their total capital stock each period. This implies that
\begin{equation*}
  S_t = (1-\delta) K_{t} + I_t \text{ and } B_t \leq \pi^n Q^n_t I_t.  
\end{equation*}
(see the proposition on page~\pageref{prop:small-interbank}). This is due to $(i)$ the fact that a large proportion of loans is the same for all banks.
% (replenishing the capital stock)
This creates only a small degree of heterogeneity in the model. $(ii)$ The proportion of net worth and deposits across islands is very similar to the proportion of credit demand across island types. There are two ways to deal with that. There needs to be more heterogeneity across firms. If firms hold \emph{long-term assets} (or they have net worth) they do not have to repay their total capital stock each period. This will narrow the gap between total assets and idiosynchratic asset on investing islands. But additionally, there should be \emph{greater heterogeneity across banks}, so there is a greater need for interbank lending. \cite{bigio2015endogenous} use firms-specific capital quality as a means to introduce greater heterogeneity.

\paragraph{Which friction matters more?} The given model features two friction parameters. There is a {general friction} $\theta_t$ and an {interbank friction} $\omega_t$. Let us interpret these parameters as measuring \emph{trust}. Given that deposits are usually ``safe'' due to deposit insurance, I assume that the friction on the deposit market plays a lesser role than the friction on the interbank market.

However, taking the model seriously, it is not \emph{trust} that matters, but the \emph{possibility to steal}, which is not affected by deposit insurance. So, one would have to make an ad-hoc assumption that bankers cannot steal depositors' money as easily as they can steal other banks' money. The incentive constraints are equilibrium conditions, ensuring that the bank does not steal assets. So even though depositors do not fear a loss (because of deposit insurance), there will be a friction---as long as banks are allowed to steal. 

One might still assume that the friction on the deposit market plays a lesser role than the friction on the interbank market. A friction on the deposit market would imply that the decrease in loans was due to a lack of deposits during the crisis. As it turns out, though, deposits did not change a lot in the crisis.
% \checkthis{ probably add plot%Monthly, Not Seasonally Adjusted, DPSACBM027NBOG,
% }
This gives rise to an alternative \ac{icc},
\begin{align*}
  V_t ( s_t^h, b_t^h, d_t) &\geq \theta_t ( Q_t^h s_t^h - \omega_tb^h_t - \kappa_t d_t) \\ 
%                           &= \omega_t (n^h_t + b_t^h + (1 - \theta_t) d_t) \\
  &= \theta_t (n^h_t + (1-\omega_t) b^h_t + (1-\kappa_t) d_t)
\end{align*}
where $\kappa_t$ and $\omega_t$ denotes the share of ``safe'' deposits and interbank lending, and the friction on the interbank market is larger than on the deposit market, $\kappa_t \gg \omega_t$.  %The effect of the interbank friction $\omega_t$ will be larger in this case than before, since it multiplies not only interbank lending, but also net worth.


\paragraph{Introduce a distinction of savings and investment banks}
An alternative route is splitting up the banks into savings and investment banks \citep[as in][]{hilberg2011asset} Since investment banks have no access to external funds, most of their lending will be funded through lending from the savings banks. A friction on this market would have a similar impact to the general friction $\theta$ in the model by \cite{gertler2011handbook}. However, the interbank market does not emerge endogenously anymore, if the island structure of the model is given up. Banks of the same type will not trade, which is in contrast to main function of the interbank market in the real world.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis2"
%%% End:
