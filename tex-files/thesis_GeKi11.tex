\mode* % needed for beamerarticle

\section{The model}
%\section{The model of \cite{gertler2011handbook}}
\label{sec:model}

  \begin{figure}[tbp]
    \centering
    \resizebox{\linewidth}{!}{
      \includegraphics{thesis-fig_geki01}
    }

    \caption{A flow chart of the model.}
    \label{fig:flow}
  \end{figure}


This section describes the ``canonical model of financial intermediation and business fluctuations'' as in \cite{gertler2011handbook}. The model extends an \ac{rbc} model with financial intermediaries (banks). As seen in figure~\ref{fig:flow} it consists of households (workers and bankers), financial intermediaries and firms. The final goods producers are competitive and the capital goods producers are subject to capital adjustment costs. Firms \emph{cannot} borrow from households directly, they have to obtain funds from banks in the form of loans.

\begin{figure}[tbp]
  \centering
  \input{fig-islands}
  \caption[Banks and islands on the unit square.]{There is a continuum of island, with a continuum of banks on each island. Each period the islands are assigned a type (investing or non-investing) independently.}
  \label{fig:islands}
\end{figure}
Household members, banks and final goods producers are located on a continuum of islands $\ell \in [0, 1]$. As illustrated in figure~\ref{fig:islands}, there is a continuum of banks $m \in [0, 1]$ and final goods producers on each island. Loans flow only within an island. There are two types of islands $h \in \{\colInv{i}, \colNon{n}\}$. Firms on investing islands \colInv{$i$} may invest, firms on non-investing islands \colNon{$n$} have to roll over the loans for their existing capital stock. The assignment\footnote{Technically, $h$ is an independent random variable $[0,1]^2 \to \{i,n\}$ such that $\Pr(h=i) = \pi^i$ and $\Pr$ is the Lebesgue measure on the unit square. \label{foot:h}} of investment opportunities is independent each period. The fraction of investing types is $\pi^{\colInv{i}}$ and  the fraction of non-investing islands is $\pi^{\colNon{n}} = 1- \pi^i$ each period.

These random investment opportunities create heterogenous liquidity needs on different island types. Banks can equilibrate their liquidity needs through an interbank market. Both the deposit and the interbank markets are subject to financial frictions because bankers get the possibility to steal a fraction $\theta$ of their assets and close the bank. They will only do so if the divertable funds exceed the value of expected future profits of the bank,
   \begin{equation*}
 \text{divertable funds} >     \text{value of continuing business} .
   \end{equation*}
This puts an endogenous bound on the banks' balance sheets (as shown in figures~\ref{fig:bal-sheet-inv} and~\ref{fig:bal-sheet-non}). Household deposits and interbank lending will be treated differently. A fraction $\omega$ of interbank lending will be regarded as ``safe''.

% While \cite{gertler2011handbook} only analyze the special cases of $\omega = 0, 1$ quantitatively, I analyze the linearized solution of the general case $\omega \in [0, 1)$. While \cite{gertler2011handbook} take regard the degrees of friction as time invariant, I will consider shocks to them: $(\omega_t, \theta_t) \sim \ar(1)$ 


In the following subsections I will describe each agent's maximization problem and the optimality conditions.


% \begin{description}
% \item[Households]
% \begin{itemize}
% \item \label{family} The households consist of big families with measure one, of which a fraction $f$ are bankers. There is turnover between bankers and workers, so that bankers do not accumulate too much net worth.

% \item \label{gamma} The household's utility is subject to habit formation with a habit formation coefficient $\gamma$. %\emph{This feature has been omitted} in the replication project in order to economize a state variable.
% \end{itemize}

% \item[Capital goods producer] \label{adjcost} The capital producers are subject to convex adjustment costs $f(I_t/I_{t-1})$ to change in investment. That is, $f(1) = 0$.

% \item[Capital stock] \label{psi} Capital is subject to an endogenous quality shock $\psi_t \sim \ar(1)$. This shock can be interpreted as obsolence of capital. Since bubbles are not modelled explicitly, the burst of the housing price bubble can be interpreted as such a shock.

% \item[Banks] \label{banks} Banks are present in the model, and they are given the opportunity to divert funds. That is they are allowed to steal a fraction $\theta$ of total assets. That is one way to model an agency friction, the key feature of models with financial frictions. The consumer (the principal) deposits his savings at the bank (the agent), expecting the bank to pay back the deposit including interest in the subsequent period. The bank's decision is out of the consumer's control. It is given the chance to break the agreement, and run away with the consumer's money. It will do that if the value of divertible funds exceeds the value of the bank\footnote{That is the sum of discounted future profits}. The lenders will thus only lend to the borrowers if they expect to be paid back, that is, only as long as the value of the bank exceeds the divertible funds.

% \item[Investment opportunities] \label{pi} Only a fraction $\pi^i$ of firms is allowed to invest each period. This creates heterogenous needs for liquidity. Formally, \citet{gertler2011handbook} introduce two different kinds of islands (investing and non-investing) with consumers, firms and agents. Firms are allowed to take out loans only from banks on the same island. That creates the need for an interbank market, where firms on non-investing island can sell their excess liquidity. This feature of the model gives rise to an interbank market where banks on non-investing islands transfer funds to banks on investing islands.
% \end{description}

% Some of those features %(\ref{family},~\ref{gamma} and~\ref{adjcost}) 
% are common in macroeconomic models. The role of banks deserves some further explanation.

\subsection{Households}
\label{sec:households}

The households are ``big families'' with a continuum of members. The household lives forever. A fraction $f$ of members are workers, $1-f$ are bankers. Every period a fraction $1 - \sigma$ of bankers become workers, and vice versa. When a banker exits, the bank's assets (terminal wealth) are transferred to her household. New bankers receive a lumpsum ``start-up transfer'' from the household. The households maximize their expected discounted lifetime utility\footnote{This formulation implies very strong
  assumptions. These include time-separable and time-invariant
  preferences, separability between consumption and labor supply,
  constant discount factor, the consumer being an expected-utility
  maximizer, the consumer having \emph{very specific} preferences
  (log-utility with habit formation).} from consumption $C_t$ and labor $L_t$ subject to their budget contraint. The household earns a wage rate $W_t$ on its labor supply, and a riskless gross return $R_{t-1}$ on deposits (savings) $D_{t-1}$ made in the previous period. There are lump-sum transfers and taxes which are summarized as a constant. The maximization problem is given by
%
 \begin{align*}
    &\max_{(C_{t}, L_{t}, D_{t})_{t}} \E_t \sum_{i=0}^\infty \beta^i \left(
    \ln (C_{t+i} - \gamma C_{t+i-1}) - \frac{\chi}{1 + \epsilon}L_{t+i}^{1+\epsilon}
\right) \\
  &\text{s.t. } C_t = W_t L_t + R_{t-1} D_{h,t-1} - D_{ht} + \text{const}
\end{align*}
$\beta$ is the discount factor; $\chi$ is the relative utility weight of labor, $\epsilon$ is the inverse of the Frisch labor elasticity and $\gamma$ is the habit formation coefficient. If $\gamma$ is non-zero, the household will have a preference for ``smooth'' consumptions paths. After exogenous shocks, consumers will avoid ``jumps'' in their consumption.

The first order conditions give rise to a standard Euler equation
\begin{align}
\E_t \underbrace{\beta \frac{u_{C,t+1}}{u_{C,t}}}_{\Lambda_{t,t+1}} R_t &= 1, \label{eq:euler}
\shortintertext{where $\Lambda_{t,t+s}$ is the stochastic discount factor, and a labor supply equation}
  \E_t u_{C_t} W_t &= \chi L_t^\epsilon \label{eq:ls},
\end{align}
where $u_{Ct}$ is the marginal utility of consumption at time $t$,
\begin{equation}
\label{eq:MU}
u_{C_t} = \frac{1}{C_t - \gamma C_{t-1}} - \beta\gamma \E_t\frac{1}{C_{t+1} - \gamma C_t}.
\end{equation}

\subsection{Final goods producers}
\label{sec:final-goods-prod}

On each island there is a continuum of final goods producers. They have an identical \ac{crs} Cobb-Douglas production technology. Since capital is heterogenous for different islands, we have to assume that labor is perfectly mobile, so that the wages are equalized.  That is why it is enough to look at the aggregate level.  Given aggregate capital stock $K_t$ and labor supply $L_t$ the output is given by
\begin{align*}
Y_t &= F(z_t, K_t, L_t)  = \exp(z_t) K_t^\alpha L_t^{1-\alpha}
\shortintertext{where $z_t$ is an exogenous productivity process}
  z_t &= \rho_z z_{t-1} + \epsilon_{z,t}. \\
\shortintertext{Each period, only those firms on investing islands may invest. Since they are chosen independently, they hold a fraction $\pi^i$ of the previous period's capital stock:}
K_t &= \underbrace{\psi_t \bigl( (1-\delta) \pi^i K_{t-1} + I_{t-1} \bigr)}_{
\text{aggregate of type \colInv{$i$}}} +  \underbrace{\psi_t (1-\delta) \pi^n K_{t-1}}_{\text{aggregate of type \colNon{$n$}}}\\
&= \psi_t \bigl( (1-\delta)K_{t-1} + I_{t-1} \bigr),\numberthis \label{eq:capital}
\shortintertext{where $\delta$ is the depreciation rate and $\psi_t$ is an exogenous process, determining the capital quality,}
\ln \psi_t &= \rho_\psi \ln \psi_{t-1} + \epsilon_{\psi,t}.
\shortintertext{Capital quality can be thought of as measuring economic obsolescence. It introduces exogenous variation in the value of capital. Firms pay the marginal product for labor and capital,}
  W_t &= F_L (z_t, K_t, L_t)  \\
  Z_t &= F_K (z_t, K_t, L_t).
\end{align*}


\subsection{The capital goods producer}

The capital goods producers produce new capital using the economy's
final output as input. The capital goods are sold to firms on
\emph{investing} islands at the price of capital $Q^i_t$. Production of capital
goods is subject to adjustment costs in the
gross rate of change in investment\footnote{The adjustment costs
  assumed here are in line with those in
  \citet[p. 15]{christiano2005nominal}. They compare their specification to previously more
  common adjustment costs in levels: ``In particular, in
  results not reported here, we found that the alternative adjustment
  cost model does not match the strong, hump-shaped response of
  investment [to a monetary policy shock] in the data.''
\citep[p. 38]{christiano2005nominal}. Their specification has since
become quite common in the literature. }.
\begin{equation}
\label{eq:adj-costs}
   f \left( \frac{I_t}{I_{t-1}}\right) = \frac{\eta_I}{2}\left(
     \frac{I_t}{I_{t-1}} - 1 \right)^2
\end{equation}
The function satisfies $f(1) = 0$ and $f'(1) = 0$, so that there are
no (marginal) adjustment costs in steady state. The adjustment costs are assumed to be
convex: $f'' = \eta_I > 0$. $\eta_I$ can be interpreted as the
inverse elasticity of net investment to the price of capital. The
capital goods producers choose a sequence of investments $(I_\tau)_{\tau =
  t}^\infty$ to maximize the discounted sum of profits:
\begin{align*}
  \max_{(I_\tau)_\tau} \E_t \sum_{\tau = t}^\infty \Lambda_{t, \tau} \Bigl(
  \underbrace{Q^i_\tau I_\tau}_\text{revenue}- \underbrace{\left( 1 + f \left(
  \frac{I_\tau}{I_{\tau-1}}\right)\right) I_\tau}_{\text{costs}}\Bigr)
\end{align*}
Capital goods producers are assumed to be owned by the
households. That is why they share the same stochastic discount factor
$\Lambda_{t,s}$. Profits (if there are any) are transferred to the
households lump-sum. The first-order necessary condition (for $I_t$) of the 
maximization problem above determines the price of assets $Q_t^i$:
\begin{equation}
\label{eq:asset-price}
  Q_t^i = 1 +  f \left( \frac{I_t}{I_{t-1}}\right)  +  
    \frac{I_t}{I_{t-1}} \cdot f' \left( \frac{I_t}{I_{t-1}}\right) -
    \E_t \Lambda_{t,t+1} \left( \frac{I_{t+1}}{I_{t}}\right)^2   f' \left( \frac{I_{t+1}}{I_{t}}\right).  
\end{equation}

\subsection{The banks and financial frictions}
\label{sec:banks}

\begin{figure}[p]\centering
 \input{fig-timing}
  \caption{Timing of the banks' problem}
\label{fig:time}
\end{figure}

\input{fig-both-bal-sheets}

Banks collect deposits from households and supply loans to firms \emph{on their island}. At the beginning of the period they choose loans $s_t^h$ and interbank lending $b_t^h$ contingently on the island type $h$, and deposits $d_t$. This can be thought of as collecting deposits \emph{before}---and giving loans \emph{after}---investment opportunities arrive (i.e.\ the island types are known). This is shown in figure~\ref{fig:time}. Once the investment opportunities have arrived, funds are scarce on the investing islands and abundant on the non-investing islands. That is why banks trade liquidity with banks of the other type (on a different island) on the interbank market. After the island type is revealed, banks execute their trades on the interbank and credit markets. It is assumed that banks borrow to the local firms frictionlessly. Funds circulate across islands only through the interbank market (see also figure~\ref{fig:flow}). %Thus, banks make a state contingent decision of credit supply and interbank trading at the beginning of each period.

Given the bank's \emph{states}\footnote{That is, the bank's choices from the period before.} $(s_{t-1}, b_{t-1}, d_{t-1})$, and bank's island type $h$, the end-of-period-$t$ net worth $n^h_t$ is given by the gross payoff of last period's assets,
%
\begin{equation}
\label{eq:net-worth}
n_t^{h} = (Z_t + (1 - \delta)Q_t^h)\psi_t s_{t - 1} - R_{bt} b_{t - 1} - R_t d_{t-1},
\end{equation}
%
where $Z_t$ are the divident payments by the non-financial firms. The firms repay the loans\footnote{In the real world, loans are debt. Here, loans are modelled as equity. The return on loans is not fixed a priori, but determined by the realizations of island type, capital quality and productivity. Unlike debt in the real world, it is thus a residual claim.} each period, subject to depreciation and a capital value shock. Note that the net worth depends on the island type only via the asset prices $Q^h_t$. Given their island type $h$ and the net worth $n^h_t$, the flow-of-funds (balance sheet) constraint for each bank is
%
\begin{equation}
\label{eq:bal-sheet}
Q_t^h s_t^h = n^h_t + b_t^h + d_t.
\end{equation}
%
This relationship is shown in figures~\ref{fig:bal-sheet-inv} and~\ref{fig:bal-sheet-non}. 

Banks pay dividends only when they exit---which happens with probability $1 - \sigma$. In that case they transfer their total (end-of-period-$t$) net worth $n^h_t$ to their household. Thus, the banks' objective function is the expected present discounted value of future dividends---the end-of-period-$t$ value of the bank:
%
\begin{equation}
  \label{eq:bank-value-sum}
  V_t = \E_{t,h} \sum_{i=0}^\infty (1-\sigma)\sigma^{i-1}\Lambda_{t,t+i}n^h_{t+i} 
\end{equation}
%The net worth $n_t^h$ depends on the island type, so it is measured at the end of each period.
 Given  the survival probability $\sigma$ in each period, the probability of exiting in period $t$ is given by $\sigma^{t-1} (1-\sigma)$. $\Lambda_{t,t+i}$ is the stochastic discount factor
\begin{equation*}
  \Lambda_{t,t+s} = \beta^s \frac{u_{C,t+s}}{u_{Ct}  },
\end{equation*}
analogous to the discount factor of the households (as defined in section~\ref{sec:households}).

The model tries to capture the fact, that banks are constrained in obtaining funds from depositors and other banks. This is motivated by the possibility of bank runs and the fact that the interbank market dried out during the Great Recession. The friction is introducing through a moral hazard problem: At the end of each period bankers have the possibility to divert funds. That is, they may transfer a share $\theta$ of divertible assets
\begin{equation*}
 Q_t^h s_t^h - \omega b_t^h = n^h_t + d_t + (1 - \omega)b_t^h
\end{equation*}
to their families. $\omega$ is the share of ``safe'' interbank assets. It measures the relative degree of friction in the interbank market. If it is zero, there is the same kind of friction for household deposits and interbank borrowing. If it is one, interbank lending is safe, so the interbank market is frictionless. The \emph{end-of-period value} of the bank $V_t(s_t^h, b_t^h, d_t)$ must be higher than the outside option (diverting), so that the banker does not have an incentive to divert funds. This is captured by an \ac{icc}
\begin{equation}
  \label{eq:bank-icc}
  V_t(s_t^h, b_t^h, d_t) \geq \theta(Q_t^h s_t^h - \omega b_t^h), \quad h \in \{i,n\}. 
\end{equation}
for each island type. The bank receives funds only as long as the \ac{icc} is satisfied, no matter which island is drawn. Otherwise the lenders will expect to lose a fraction $\geq \theta$ of their funds.

It will be shown that the banks' end-of-period value function is linear
in the choices,
\begin{equation}
  \label{eq:linear-value}
  V_t(s_t^h, b_t^h, d_t) = \V_{st} s_t^h - \V_{bt} b_t^h - \V_{dt} d_t,
\end{equation}
$\V_{st}$ is the marginal value of additional loans to firms,
$\V_{bt}$ and $\V_{dt}$ are the marginal costs of deposits by
households and interbank borrowing, respectively\footnote{Here the notation differs from
  \cite{gertler2011handbook}. Their $\V_t$ is my $\V_{dt}$.}. The
bank's problem is formally summarized below. The details are found in appendix~\ref{app:banks-problem}.

% %The paper assumes that the incentive compatibility constraint is alsways binding.
% In the case of a perfect interbank market ($\omega = 1$) it is shown in the paper that the aggregate ICC is
% \begin{equation}
%   \label{eq:icc}
%   Q_t S_t = \phi_t N_t, \quad \text{where } \phi_t = \frac{\V_{dt}}{\theta - \frac{\V_{st}}{Q_t} + \V_{dt}},
% \end{equation}
% $Q_t = Q^i_t = Q^n_t$ is the price of capital, $S_t$ and $N_t$ are the aggregate levels of assets and net worth, respectively. For later reference note that $\partial \phi_t / \partial Q_t < 0$.

\subsubsection{The banks' problem}

The banks maximize their expected terminal wealth~\eqref{eq:bank-value-sum}, subject to the constraints~\eqref{eq:net-worth}, \eqref{eq:bal-sheet} and~\eqref{eq:bank-icc}.

Let $V(s_t, b_t, d_t)$ be the banks' \emph{end-of-period-$t$} value function, depending on the choices in period $t$. Given equation~\eqref{eq:bank-value-sum} it can be written in recursive form as
%
\begin{equation}
\label{eq:bellman}
V(s_t, b_t, d_t) = \E_t \Lambda_{t,t+1} \sum_h \pi^h \Bigl( (1-\sigma)
 n^h_{t+1} +\sigma \max_{s_{t+1}^{h}, b^{h}_{t+1}, d_{t+1} }
                    V(s_{t+1}^{h}, b^{h}_{t+1}, d_{t+1}) \Bigr),
\end{equation}
where the choice is subject to the \ac{icc}
constraint~\eqref{eq:bank-icc}. The end-of-period problem can be written
as a Lagrangian, using the Lagrange multipliers
$\lambda_t^h$ for the \ac{icc} constraint,
\begin{align*}
  \La_{t} &= V(s_{t}^{h},
  b^{h}_{t}, d_{t}) +
  \lambda_{t}^h  \Bigl( V(s_{t}^h, b_{t}^h, d_{t}) - \theta(Q_{t}^h s_{t}^h
  - \omega b_{t}^h)\Bigr) \\
&=(1+\lambda_{t}^h) V(s_{t}^{h},
  b^{h}_{t}, d_{t}) - \lambda_{t}^h \theta (Q_{t}^h s_{t}^h
  - \omega b_{t}^h) \numberthis \label{eq:banks-lag} \\
 \end{align*}
Using complementary slackness, one can summarize the banks problem as
\begin{align*}
  & \begin{aligned}
      V(s_{t-1}, b_{t-1}, d_{t-1}) = &\E_{t-1} \Lambda_{t-1,t} \sum_h
      \pi^h \Bigl( (1-\sigma) n^h_{t} +\sigma \max_{s_{t}^{h}, b^{h}_{t}, d_{t} } (1+\lambda_{t}^h) V(s_{t}^{h},
      b^{h}_{t}, d_{t}) \\
 &\qquad- \lambda_{t}^h \theta (Q_{t}^h s_{t}^h
      - \omega b_{t}^h)
      \Bigr)
    \end{aligned}\numberthis \label{eq:bellman2} \\
&\text{s.t. } n_{t+1}^{h} = (Z_{t+1} + (1 -
  \delta)Q_{t+1}^{h})\psi_{t+1} s_{t} - R_{bt+1} b_{t} - R_{t+1} d_{t}
,
\end{align*}

The first order necessary conditions, the verification of the linear
value function and more details are found in appendix~\ref{app:banks-problem}. 

\subsubsection{The incentive constraint and credit spreads}
\label{sec:spreads}
Building on the first order necessary conditions (derived in appendix~\ref{app:foc}) the guessed value function is verified in appendix~\ref{app:general-case}. The undetermined coefficients are given by
\begin{align*}
\V_{st} &= \E_t \Lambda_{t,t+1} \sum_{h} \pi^{h} \Omega^{h}_{t+1} \bigl(Z_{t+1} + (1 - \delta)Q_{t+1}^{{h}}\bigr)\psi_{t+1}, \numberthis \label{eq:Vs-gen} \\
\V_{bt} &= R_{bt} \cdot \E_t \Lambda_{t,t+1} \sum_{h} \pi^{h} \Omega^{h}_{t+1},  \numberthis \label{eq:Vb-gen} \\
\V_{dt} &= R_t \cdot\E_t \Lambda_{t,t+1} \sum_{h} \pi^{h} \Omega^{h}_{t+1} = \frac{R_t}{R_{bt} }\V_{bt}. \numberthis \label{eq:Vd-gen} \\
\intertext{with $1-\sigma$ being the probability the banker exits and}
\Omega_{t+1}^h &= 
  1-\sigma +\sigma \bigl(\V_{bt+1} + \lambda_{t+1}^h
  (\V_{bt+1} - \theta\omega)\bigr). \numberthis \label{eq:Omega-gen}
\end{align*}

{As shown in appendix~\ref{app:general-case}, the value function has a different representation~\eqref{eq:bank-gen-value} in terms of net worth. Then, $\Omega_{t+1}^h$ is the marginal value of net worth of bankers on island type $h$.}

Combining these equations with equations~\eqref{eq:14} and~\eqref{eq:15}---which are combinations of the first order conditions---reveals how the \acp{icc}~\eqref{eq:bank-icc} relate to interest rate spreads:
\begin{align*}
\frac{\theta \omega \bar \lambda_t}{1 + \bar \lambda_t} &= (R_{bt} - R_t) \cdot \E_t \Lambda_{t,t+1} \sum_{h} \pi^{h} \Omega^{h}_{t+1} \\
\frac{\lambda_t^h \theta (1 - \omega)}{1 + \lambda_t^h}&= \E_t \Lambda_{t,t+1} \sum_{h} \pi^{h} \Omega^{h}_{t+1} ( R_{k,t+1}^{\tilde hh}  - R_{bt} )
\intertext{where}
  R_{k,t+1}^{\tilde hh} &= \frac{Z_{t+1} + (1 - \delta)Q_{t+1}^{{h}}}{Q_t^{\tilde h}} \psi_{t+1}
%\intertext{}
\end{align*}
is the return to bank lending depending on the current island type
$\tilde h$ and the island type next period $h$.

Note that the returns on interbank lending $R_{bt}$ and household deposits $R_t$ are determined in the period before they are paid. $\lambda_t^h$ are Lagrange multipliers for the incentive compatibility constraint for each island type for $h \in \{ i, n\}$, and thus non-negative. $\bar \lambda_t$ is their average:
\begin{equation}
  \label{eq:lambdabar}
  \bar \lambda_t = \sum_h \pi^h \lambda^h_t.
\end{equation}

By the complementary slackness conditions of a constrained optimum, the multiplier is non-zero only if the contraint binds. Since for a given degree of friction $\theta \neq 0$ and $\omega \neq 0, 1$ a higher spread is equivalent to a higher multiplier\footnote{That is because $\displaystyle \frac{\partial }{\partial \lambda} \frac{\lambda}{1 + \lambda} = \frac{1}{(1+\lambda)^2} > 0$.}, a higher spread implies a more tightly binding \ac{icc}.

On the other hand,  if the constraint is not binding on an island, the multiplier must be zero. So there will be no credit spread on this island. If the constraint binds on neither island, the bank lending spread will be zero, too: $R_{bt} = R$.

The above equations also show that if there is no general friction ($\theta = 0$, no funds can be diverted), there will be no spreads at all. If there is symmetric friction on the interbank and deposit markets ($\omega = 0$, interbank lending is not safe), there will be no spread between deposits and interbank lending. If there is no friction on the interbank market ($\omega = 1$, interbank lending is safe) there will be no spread between the credit and interbank markets. 

\subsubsection{Evolution of aggregate net worth}

\def\island{\vec 1_h(h_j)}
\def\differ{\dd \lambda(j)}
There is a high degree of heterogeneity across islands. The islands might have different histories of investment opportunities. For example, two islands of type $i$ are different if in the previous period one was of type $i$ and one of type $n$. In this section I will show that one can aggregate over islands of one type, because only aggregate quantities of the previous period matter.

This is due to the island structure shown in figure~\ref{fig:islands}, since the investment opportunities are drawn independently\footnote{That is, the random variable $h$ described in footnote~\ref{foot:h} is independent.}. But we have to make an additional assumption.

\begin{assumption}[Rates of return are equal across islands]
Like \citet{gertler2011handbook}, I make an assumption about banks' rates of return in order to get tractability. In particular, banks are allowed to change islands at the beginning of each period so that ex ante rates of return are equal across islands (arbitrage). This happens the following way: A fraction of banks on islands with low return move to islands with high return. They sell their loans to other banks that remain on the island in exchange for interbank loans. 
%\emph{It is not so clear how that happens}. It will be shown later that equalized returns require the ratio of intermediary net worth to total capital being equal on each island.
\end{assumption}
%
 Let aggregate loans $S_{t-1}$, deposits $D_{t-1}$ and $B_{t-1} = 0$ be the integral over all banks for banks $j \in [ 0, 1 ]^2$, e.g.\
 \begin{equation*}
   S_{t-1} = \int_{[0,1]^2} s_{j,t-1} \differ
 \end{equation*}
 aggregate net worth of islands of type $h$ can then be written as
\begin{equation}
%\resizebox{1.\linewidth}{!}{$
  \begin{aligned}
    \tilde N^h_t &= \int_{[0,1]^2}\island n_{j,t} \differ 
\shortintertext{since $h$ is independent,}
&=    \underbrace{\int_{[0,1]^2}\island\differ}_{P(h_j = h) = \pi^h}
    \int_{[0,1]^2} n_{j,t} \differ =
 \pi^h \int_{[0,1]^2} n_{j,t} \differ \\
\shortintertext{plug in for net worth, and assume that returns equal across islands,}
    &= \pi^h \int_{[0,1]^2} \bigl(Z + (1 - \delta) Q^h_t\bigr)
    \psi_t s_{j,t-1} - R_{b,t-1} b_{j,t-1} - R_{t-1} d_{j,t-1}
    \differ \\
&= \pi^h  \Bigl(\bigl(Z + (1 - \delta) Q^h_t\bigr)
    \psi_t S_{t-1} - R_{b,t-1} \underbrace{B_{t-1}}_{=0} - R_{t-1} D_{t-1}\Bigr).
  \end{aligned}
%$}
\end{equation}
the interbank market has to net out on aggregate. This is the net worth \emph{before} bankers learn if they stay or exit. Only a fraction $\colc{\sigma}$ of bankers survives. They are replaced by new bankers, who receive a \emph{start-up transfer} from their household. \citet{gertler2011handbook} assume that the transfer is a fraction $\frac{\xi}{1 - \sigma}$ of total assets of exiting bankers
\begin{equation*}
  \frac{\xi}{1 - \sigma}(1-\sigma) \pi^h \bigl( Z_t + (1-\delta) Q_t^h \bigr) \psi_{t}   S_{t-1}.
\end{equation*}
This gives a nice expression for aggregate net worth on each island:
\begin{align*}
  N_t^h &= \underbrace{\colc{\sigma} \pi^h \Bigl(\bigl(Z + (1 - \delta) Q^h_t\bigr) 
    \psi_t S_{t-1} - R_{t-1} D_{t-1}\Bigr)}_{\text{net worth of surviving bankers}}
+ \underbrace{\xi \pi^h \bigl(Z + (1 - \delta) Q^h_t\bigr) 
    \psi_t S_{t-1}}_{\text{start-up transfer}}\\
&= \pi^h \Bigl(\bigl(Z + (1 - \delta) Q^h_t\bigr) 
    \psi_t S_{t-1} (\sigma  + \xi) - \sigma  R_{t-1} D_{t-1}\Bigr). \numberthis \label{eq:agg-net}
\end{align*}

Note that net worth depends on asset returns $(Z_t + ( 1 - \delta)
Q_t^h) \psi_t$. That impact will be greater, the higher the banks
leverage $\frac{Q^h_tS^h_t}{N^h_t}$. In particular, a decline in capital quality $\psi_t$ will directly reduce
net worth.


  


%\subsubsection{The evolution of net worth}
%\label{sec:evolution-net-worth}
% If all bankers of period $t-1$ survived to period $t$ the aggregate
% net worth would be the gross earnings from the previous period:
% \begin{equation*}
%  \bigl( Z_t + (1-\delta) Q_t^h \bigr) \psi_{t} \pi^h
%   S_{t-1} - R_{t-1} D_{t-1}.
% \end{equation*}
% This follows from the law of motion of individual banks' net
% worth~\eqref{eq:net-worth}. Aggregate deposits and loans are
% \begin{equation*}
% D_{t-1} = d_{t-1} \quad\text{ and }\quad S_{t-1} = \pi^i s^i_{t-1} + \pi^n
% s^n_{t-1},
% \end{equation*}
% respectively\footnote{$D_{t-1}
% = \int d_{t-1} = d_{t-1}$ and $S_{t-1} = \pi^i s^i_{t-1} + \pi^n
% s^n_{t-1}$ \checkthis{elaborate!}}. Interbank lending $b^h_{t-1}$ nets out in
% aggregate. However, as pointed out earlier, bankers do not stay
% bankers forever. This is to avoid accumulation of too much net
% worth. Every period a fraction $\sigma$ of bankers exits and is
% replaced by new bankers. So
% for each island type $h \in \{ i, n \}$, the total net worth is given by
% \begin{equation*}
%   N^h_t = N^h_{ot} + N^h_{yt}, 
% \end{equation*}
% where $o$ is for old and $y$ for young bankers.  Since the fraction of
% exiting bankers is the same on all islands, and the investment shocks
% (draw of new island type) are independent of previous island type, the
% initial distribution of loans and deposits will be equal across islands. 
% \begin{equation*}
%   N^h_{to} = \sigma \pi^h \left(\bigl( Z_t + (1-\delta) Q_t^h \bigr) \psi_{t} S_{t-1} - R_{t-1} D_{t-1} \right)
% \end{equation*}
% Combining the last three equations yields
% \begin{equation}
%   \label{eq:agg-net-worth}
%   N_t^h = (\sigma + \xi) \pi^h \bigl( Z_t + (1-\delta) Q_t^h \bigr)
%   \psi_{t} S_{t-1} -  \sigma \pi^h R_{t-1} D_{t-1}
% \end{equation}
% Additionally, there is an aggregate balance sheet constraint,
% \begin{equation}
%   \label{eq:agg-bal-sheet}
%   D_t = \sum_{h \in \{ i ,n \}} Q_t^h S^h_t - N_t^h.
% \end{equation}



\subsection{Market clearing and equilibrium}
\label{sec:equilibrium}

Since interbank lending nets out in aggregate, bank loans $S^h_t$
    are financed by deposits $D_t$ and total net worth $N_t$,
  \begin{equation*}
    \sum_{h \in \{i,n\}} Q_t^h S_t^h = D_t + N_t
  \end{equation*}

In equilibrium, the loan and labor markets clear. Firms have to roll over loans for their total capital stock each period. So total loans equals total capital:
    \begin{align}
\left.
      \begin{aligned}
        S^i_t &= I_t + (1-\delta) \pi^i K_t \\
        S^n_t &= (1-\delta) \pi^n K_t
      \end{aligned}
\right\} \implies S_t = I_t + (1-\delta) K_t = K_{t+1} \label{eq:macl}
    \end{align}

Market clearing on the labor market implies that
\begin{equation}
  \label{eq:ls}
  \underbrace{F_L ( z_t, K_t, L_t)}_{= W_t}  u_{Ct} = \chi L_t^\epsilon.
\end{equation}
Finally, the aggregate resource constraint is given by
\begin{equation}
  \label{eq:arc}
  Y_t = C_t + \left(1+f\left(\frac{I_t}{I_{t-1}}\right)\right) I_t + G_t.
\end{equation}
where $G_t$ is the government expenditure which is calibrated to be $20\%$ of output in steady state, $G_t = 0.2 \cdot Y^*$.

% \begin{frame}
% \frametitle{Discussion}
% \begin{description}[Critique]
% \item[Mechanism] tighter constraint, fire sales: large drop in net worth
% \item[Policy response] government makes up with direct credit, relaxes
%   ICC, reduces spreads
% \item[Critique] 
% \begin{itemize}
% \item no quantitative evaluation of other policies in the paper
% \item all policies are financed by short term debt

% i.e.\ there are no long-term government bonds
% \end{itemize}
% \end{description}


% \end{frame}
  


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis2"
%%% End:
