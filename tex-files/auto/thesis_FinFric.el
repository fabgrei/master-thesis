(TeX-add-style-hook
 "thesis_FinFric"
 (lambda ()
   (LaTeX-add-labels
    "sec:literature"
    "sec:empir-lit"
    "sec:theor-lit")))

