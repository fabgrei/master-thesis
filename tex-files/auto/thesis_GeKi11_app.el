(TeX-add-style-hook
 "thesis_GeKi11_app"
 (lambda ()
   (LaTeX-add-labels
    "app:banks-problem"
    "app:foc"
    "eq:foc-s"
    "eq:foc-b"
    "eq:foc-d"
    "eq:foc-l"
    "eq:14"
    "eq:15"
    "eq:16"
    "sec:interpretations"
    "eq:levRat"
    "eq:Vds1"
    "eq:Vdn1"
    "eq:Vd1"
    "eq:mu1"
    "eq:omega1"
    "app:general-case"
    "eq:14b"
    "eq:15b"
    "eq:agg-ICC"
    "eq:74"
    "eq:75"
    "eq:bank-gen-value")))

