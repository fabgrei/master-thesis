(TeX-add-style-hook
 "thesis"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "x11names" "dvipsnames" "a4paper")))
   (TeX-run-style-hooks
    "latex2e"
    "headerFiles/header"
    "headerFiles/math-def"
    "headerFiles/tikz-def"
    "headerFiles/acronyms"
    "thesis_intro"
    "thesis_FinFric"
    "thesis_GeKi11"
    "thesis_GeKa13"
    "thesis_backmatter"
    "thesis_GeKi11_app"
    "thesis_appendix"
    "article"
    "art10"
    "beamerarticle"
    "geometry"
    "array"
    "booktabs"
    "colortbl"
    "ctable"
    "caption")
   (TeX-add-symbols
    "foo")
   (LaTeX-add-labels
    "sec:literature")
   (LaTeX-add-environments
    "stylfact")
   (LaTeX-add-bibliographies
    "headerFiles/mabib")))

