(TeX-add-style-hook
 "thesis_app_proof"
 (lambda ()
   (TeX-add-symbols
    "assN"
    "assI")
   (LaTeX-add-labels
    "app:proof-small-interbank"
    "eq:assets"
    "eq:liab")))

