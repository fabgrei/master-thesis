(TeX-add-style-hook
 "presentation"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "ignorenonframetext" "mathserif" "red")))
   (TeX-run-style-hooks
    "latex2e"
    "headerFiles/header"
    "headerFiles/math-def"
    "headerFiles/tikz-def"
    "headerFiles/acronyms"
    "beamer"
    "beamer10")
   (LaTeX-add-environments
    "stylfact")
   (LaTeX-add-bibliographies
    "headerFiles/mabib")))

