(TeX-add-style-hook
 "thesis-fig_geki01"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("standalone" "x11names")))
   (TeX-run-style-hooks
    "latex2e"
    "tikz-def"
    "standalone"
    "standalone10"
    "fullpage"
    "marvosym")))

