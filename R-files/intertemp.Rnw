\documentclass{article}


\begin{document}




<<interbankdata, dev='tikz', fig.cap='Total bank credit and interbank lending by US commerical banks. The shaded areas denote recessions. Source: FRED', fig.height=6, out.width='\\linewidth', echo=F, results=F, warnings=F>>=
series <- c(interbank='IBLACBW027NBOG', total='TOTBKCR')
require(quantmod)
symNames <- getSymbols(series, src = 'FRED')


credit1 <- cbind("interbank loans weekly"=(IBLACBW027NBOG), "total bank credit"=TOTBKCR)
credit2 <- credit1["1998/"] 
require(data.table)
credit3 <- as.data.table(credit2)
credit3 <- credit3[, ratio := IBLACBW027NBOG/TOTBKCR]
setnames(credit3, old=symNames, new=c("interbank loans (stock)", "total bank credit (stock)"))
require(reshape2)
credit4 <- melt(data=credit3, id.vars="index")

minmax <- summary(credit3$ratio)[c("Min.", "Max.")]
minmax <- round(minmax*100, 1)
minmaxout <- paste("$",minmax,"\\%$", sep="", collapse=" and ")

p <- ggplot(credit4, aes(x=index))
suppressWarnings(p <- nberShade(p, xrange = c("1998-01-01", "2010-01-01")))
p + geom_line(aes(y=value)) + theme(legend.position="top") + facet_wrap(~variable, ncol=1, scales="free_y") + labs(x=NULL, y="in billion US dollars")

@ 

The interbank market is a whole sale market, where banks trade
liquidity. A big chunk of interbank lending is overnight
lending. Banks use this instrument to satisfy their reserve
requirements. These credits are usually paid back within a day, so
they do not show up in banks balance sheets (which only show
the \emph{level} of outstanding credit as parts of assets and
borrowing as parts of liabilities). That is why data on overnight
trade is hard to get.

Figure~\ref{fig:interbankdata} shows the \emph{balance sheet level} of interbank credit and
total credit for US commercial banks. It can be seen that the
interbank loans is between \Sexpr{minmaxout} of total credit. The steady state
values of our model show that this ratio is of a similar magnitude:
$\Sexpr{round(0.11/6.1*100, 1)}\%$. However, it can be seen in the
lowest panel of figure~\ref{fig:interbankdata} that the interbank
market was much bigger before the crisis than it is now. A
macroeconomic model that wants to explain the Great Recession should
probably aim at matching the levels before the crisis.

Still, even before the crisis, interbank lending was only a small fraction of total bank
credit on banks' balance sheets. But an important dimension of the
interbank market is not mirrored by the balance sheets: the overnight
lending. \citet[table 1]{Demiralp2004} estimate the \emph{daily volume} of $\$145$
billion of overnight lending for the first quarter of 1998. That is,
the flows of interbank lending are of a much higher magnitude than the
balance sheet stocks: on average, around $\Sexpr{round(145/250*100,0)}\%$ of the
stock was traded.

