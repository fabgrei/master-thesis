function [residual, g1, g2, g3] = test_dynamic(y, x, params, steady_state, it_)
%
% Status : Computes dynamic model for Dynare
%
% Inputs :
%   y         [#dynamic variables by 1] double    vector of endogenous variables in the order stored
%                                                 in M_.lead_lag_incidence; see the Manual
%   x         [M_.exo_nbr by nperiods] double     matrix of exogenous variables (in declaration order)
%                                                 for all simulation periods
%   params    [M_.param_nbr by 1] double          vector of parameter values in declaration order
%   it_       scalar double                       time period for exogenous variables for which to evaluate the model
%
% Outputs:
%   residual  [M_.endo_nbr by 1] double    vector of residuals of the dynamic model equations in order of 
%                                          declaration of the equations
%   g1        [M_.endo_nbr by #dynamic variables] double    Jacobian matrix of the dynamic model equations;
%                                                           rows: equations in order of declaration
%                                                           columns: variables in order stored in M_.lead_lag_incidence
%   g2        [M_.endo_nbr by (#dynamic variables)^2] double   Hessian matrix of the dynamic model equations;
%                                                              rows: equations in order of declaration
%                                                              columns: variables in order stored in M_.lead_lag_incidence
%   g3        [M_.endo_nbr by (#dynamic variables)^3] double   Third order derivative matrix of the dynamic model equations;
%                                                              rows: equations in order of declaration
%                                                              columns: variables in order stored in M_.lead_lag_incidence
%
%
% Warning : this file is generated automatically by Dynare
%           from model file (.mod)

%
% Model equations
%

residual = zeros(5, 1);
T16 = exp(y(9))^(-params(5));
T28 = params(1)*exp(y(8))*exp(y(5))^(params(1)-1)+1-params(3);
T37 = exp(y(6))*exp(y(1))^params(1);
T58 = exp(y(6))*exp(y(1))*getPowerDeriv(exp(y(1)),params(1),1);
lhs =exp(y(7))^(-params(5));
rhs =params(2)*T16*T28;
residual(1)= lhs-rhs;
lhs =exp(y(5));
rhs =T37-exp(y(7))+(1-params(3))*exp(y(1));
residual(2)= lhs-rhs;
lhs =y(6);
rhs =params(4)*y(2)+x(it_, 1);
residual(3)= lhs-rhs;
lhs =exp(y(3));
rhs =T37;
residual(4)= lhs-rhs;
lhs =exp(y(4));
rhs =exp(y(3))-exp(y(7));
residual(5)= lhs-rhs;
if nargout >= 2,
  g1 = zeros(5, 10);

  %
  % Jacobian matrix
  %

  g1(1,5)=(-(params(2)*T16*params(1)*exp(y(8))*exp(y(5))*getPowerDeriv(exp(y(5)),params(1)-1,1)));
  g1(1,8)=(-(params(2)*T16*params(1)*exp(y(8))*exp(y(5))^(params(1)-1)));
  g1(1,7)=exp(y(7))*getPowerDeriv(exp(y(7)),(-params(5)),1);
  g1(1,9)=(-(params(2)*T28*exp(y(9))*getPowerDeriv(exp(y(9)),(-params(5)),1)));
  g1(2,1)=(-((1-params(3))*exp(y(1))+T58));
  g1(2,5)=exp(y(5));
  g1(2,6)=(-T37);
  g1(2,7)=exp(y(7));
  g1(3,2)=(-params(4));
  g1(3,6)=1;
  g1(3,10)=(-1);
  g1(4,3)=exp(y(3));
  g1(4,1)=(-T58);
  g1(4,6)=(-T37);
  g1(5,3)=(-exp(y(3)));
  g1(5,4)=exp(y(4));
  g1(5,7)=exp(y(7));
end
if nargout >= 3,
  %
  % Hessian matrix
  %

  g2 = sparse([],[],[],5,100);
end
if nargout >= 4,
  %
  % Third order derivatives
  %

  g3 = sparse([],[],[],5,1000);
end
end
